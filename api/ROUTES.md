# MarwynnBot API Routes Documentation
This document details all routes MarwynnBot's internal API cog should be
able to handle, including information about each route's endpoint, 
available HTTP methods, and suggested function names, if necessary.


# Adding Routes
The `api` module provides several methods to add a route to the internal API
server, namely `@add_route`. Documentation for that may be found [here](
https://gitlab.com/marwynnbot-developers/marwynnbot/-/blob/development/api/server.py#L282
).


# Authentication Protocol
Please note that the routes listed here and implemented in the API cog
are not official API routes. The user will not directly query these routes,
as they will be doing so through https://bot.marwynn.me/api. Authentication
needs to be done before access to resources is granted. Every route listed here
and implemented in the API cog should assume an authenticated request has been
sent to the webserver first, and that any request send to this API is a proxied
request from the webserver.

## Extra Checks
The webserver shall issue each request with a predefined `MB-MASTER-API-KEY`
header. Each API route shall check this predefined header to ensure a match,
only then it will return any meaningful resources. This functionality shall
be implemented during runtime, therefore developers should not need to implement
this functionality directly into the request handler itself. However, there
may be instances where extra checking is required. This shall be done through
the use of the `@check` decorator. Documentation for that may be found [here](
https://gitlab.com/marwynnbot-developers/marwynnbot/-/blob/development/api/server.py#L301
).

# Extraneous Request Data
Other than the request data specified in this document, all extraneous data is
to be ignored. This includes, but is not limited to request headers, query string
parameters, etc.


# Routes
This section will describe all routes the API should implement, including info
on the HTTP methods, paths, return schemas, and status codes. The paths described
are paths that will be appened to the base url, https://bot.marwynn.me/api.
Please note that end slashes are not strict.

## GET /
Base route, will be replaced

Returns:
```json
{
    "status": 200,
}
```

Status Codes: `200`

## GET /uptime
Fetches the bot's uptime in seconds

Returns:
```json
{
    "uptime": seconds,
}
```

Status Codes: `200`

## GET /status
Fetches the bot's aggregate stats

Returns:
```json
{
    "commands": command_count,
    "guilds": guild_count,
    "users": users_count,
}
```

Status Codes: `200`

## GET /commands
Fetches the bot's command count

Returns:
```json
{
    "commands": command_count,
}
```

Status Codes: `200`

## GET /guilds
Fetches the bot's guild count

Returns:
```json
{
    "guilds": guild_count,
}
```

Status Codes: `200`

## GET /users
Fetches the bot's user count

Returns:
```json
{
    "users": users_count,
}
```

Status Codes: `200`

# Implementation Guide
The following routes below should be added to the API cog

## GET /command
Gets information on a command

Query Parameters:
|     Param     |                    Description                     |  Type  | Defaults |
| :-----------: | :------------------------------------------------: | :----: | :------: |
|    `name`     |              The name of the command               | `str`  |   N/A    |
| `subcommands` | Flag to set to retrieve subcommand data from query | `bool` | `False`  |

Returns:
A command object

```json
{
    "command": {
        "name": command.name,
        "description": kwargs['desc'],
        "usage": command.usage,
        "aliases": command.aliases | null,
        "user_perms": kwargs['uperms'] | null,
        "bot_perms": kwargs['bperms'] | null,
        "note": kwargs['note'] | null,
        "subcommands": [
            "command": {...},
        ] | null,
    }
}
```

Upon invalid query or command does not exist,
```json
{
    "message": "The command could not be found",
    "payload": {
        "name": name,
        "subcommands": subcommands,
    },
}
```

Status Codes:
- On successful query: `200`
- On failed query: `404`

## GET /guild
Gets information about a guild

Query Parameters:
|   Param    |                    Description                     |  Type  | Defaults |
| :--------: | :------------------------------------------------: | :----: | :------: |
|    `id`    |                The ID of the guild                 | `int`  |   N/A    |
| `detailed` | Flag to set to retrieve detailed guild information | `bool` | `False`  |

Checks:
- Ensure `id` is an integer
  - On failure, return
    ```json
    {
        "message": f"id {id} is not an integer",
        "payload": {
            "id": id,
            "detailed": detailed,
        },
    }
    ```
- Ensure `detailed` is either "true" or "false" (case insensitive)
  - On failure, return
    ```json
    {
        "message": f"detailed is not either \"true\" or \"false\"",
        "payload": {
            "id": id,
            "detailed": detailed,
        },
    }
    ```

Returns:
A guild object

```json
{
    "guild": {
        "id": guild.id,
        "name": guild.name,
        "member_count": guild.member_count,
        "icon_url": guild.icon_url | null,
    },
}
```

If `detailed` is `True`,
```json
{
    "guild": {
        "id": guild.id,
        "name": guild.name,
        "large": guild.large,
        "created_at": guild.created_at,
        "member_count": guild.member_count,
        "role_count": len(guild.roles),
        "emoji_count": len(guild.emojis),
        "icon_url": guild.icon_url | null,
        "text_channels": len(guild.text_channels),
        "voice_channels": len(guild.voice_channels),
        "category_channels": len(guild.channels),
        "owner_id": guild.owner_id | null,
        "shard_id": guild.shard_id,
    },
}
```

If MarwynnBot is not in a guild with the specified `id`, or the guild
could not be found:
```json
{
    "message": f"A guild with the ID {id} was not found",
    "payload": {
        "id": id,
        "detailed": detailed,
    },
}
```

Status Codes:
- On successful query: `200`
- On check validation error: `400`
- On guild not found: `404`