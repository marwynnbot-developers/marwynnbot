import asyncio
import datetime
import functools
import json
import os
import traceback
from typing import Callable, List, NamedTuple, Optional, Union

import discord
import yarl
from aiohttp import ClientResponseError, ClientSession, web
from bs4 import BeautifulSoup
from discord.ext import commands
from discord.ext.commands import AutoShardedBot, Context
from setuppanel import SetupPanel
from utils import EmbedPaginator
from utils.customerrors import SilentButDeadly

_BASE = "https://www.youtube.com/xml/feeds/videos.xml?channel_id={}"
_BC = lambda m: m.content == "cancel"
_CONF = ["✅", "❌"]
_FC = "Enter \"cancel\" to cancel the {}"
_MG = ["Manage Server"]
_SUB = "https://pubsubhubbub.appspot.com/subscribe"
_YT_TS = "The following template strings will be replaced by data from the video:\n" + \
    "\n".join([
        "> `{{author}}` - name of the channel the video was uploaded to",
        "> `{{title}}` - title of the video",
        "> `{{url}}` - the URL of the video",
        "> `{{channel_url}}` - the URL of the channel the video was uploaded to",
    ])
_TWITCH_TS = "The following template strings will be replaced by data from the twitch stream:\n" + \
    "\n".join([
        "> `{{author}}` - name of the Twitch",
        "> `{{title}}` - title of the twitch stream",
        "> `{{game}}` - game the Twitch is streaming",
        "> `{{url}}` - the URL of the twitch stream",
    ])
_TWITCH_BASE = "https://api.twitch.tv/helix/{}"
_TWITCH_CALLBACK = os.getenv("TWITCH_CALLBACK")
_TWITCH_CID = os.getenv("TWITCH_CLIENT_ID")
_TWITCH_CS = os.getenv("TWITCH_CLIENT_SECRET")
_TWITCH_SECRET = os.getenv("TWITCH_REQ_SECRET")
_YTAPI = os.getenv("YTAPIKEY")


def raise_handler(title: str = None, description: str = None):
    def wrapper(func: Callable):
        @functools.wraps(func)
        async def deco(self, ctx: Context, *args, **kwargs):
            try:
                await func(self, ctx, *args, **kwargs)
            except ClientResponseError as e:
                return await ctx.channel.send(
                    embed=discord.Embed(
                        title=title or f"Error Code {e.code}",
                        description=description or f"An error occurred while processing the request: ```{e.message}```",
                        color=discord.Color.dark_red(),
                    )
                )
        return deco
    return wrapper


class YouTubeChannel(NamedTuple):
    id: str
    title: str
    thumbnail_url: str


class TokenData(NamedTuple):
    token: str
    token_type: str
    expire_at: int


class TwitchUser(NamedTuple):
    id: str
    login: str
    display_name: str
    type: str
    broadcaster_type: str
    description: str
    profile_image_url: str
    offline_image_url: str
    view_count: str
    created_at: str

    @property
    def stream_url(self) -> str:
        return f"https://twitch.tv/{self.display_name}"


class TwitchStream(NamedTuple):
    game: str
    title: str
    display_name: str
    url: str
    thumbnail_url: str


class Notifications(commands.Cog):
    def __init__(self, bot: AutoShardedBot) -> None:
        self.bot = bot
        self.client_session = ClientSession()
        self.web = web.Application(loop=self.bot.loop)
        self._twitch_token: TokenData = None
        self._tasks: List[asyncio.Task] = []
        for func in [self._init_aiohttp, self._init_tables]:
            self._tasks.append(
                self.bot.loop.create_task(func())
            )

    def cog_unload(self):
        for func in [self.site.stop, self.runner.shutdown, self.client_session.close]:
            task = self.bot.loop.create_task(func())
            task.add_done_callback(self._handle_task_result)
        for task in self._tasks:
            task.cancel()

    async def _init_aiohttp(self) -> None:
        await self.bot.wait_until_ready()
        self.web.add_routes([
            web.post("/youtube", self.handle_yt_pub),
            web.post("/twitch", self.handle_twitch_pub),
        ])
        self.runner = web.AppRunner(self.web)
        await self.runner.setup()
        host = os.getenv("PUBSUBHOST")
        port = int(os.getenv("PUBSUBPORT"))
        self.site = web.TCPSite(self.runner, host=host, port=port)
        await self.site.start()
        print(f"NOTIFICATIONS AIOHTTP SERVER STARTED, LISTENING ON {host}:{port}")

    async def _init_tables(self) -> None:
        await self.bot.wait_until_ready()
        async with self.bot.db.acquire() as con:
            await con.execute(
                "CREATE TABLE IF NOT EXISTS ytsub(channel_id TEXT PRIMARY KEY, expire_at NUMERIC)"
            )
            await con.execute(
                "CREATE TABLE IF NOT EXISTS ytsubconfig(guild_id BIGINT, channel_id BIGINT, "
                "yt_channel_id TEXT, webhook_id BIGINT, template TEXT)"
            )
            await con.execute(
                "CREATE TABLE IF NOT EXISTS ytsubvideoids(video_id TEXT PRIMARY KEY)"
            )
            await con.execute(
                "CREATE TABLE IF NOT EXISTS twitchaccess(token TEXT PRIMARY KEY, type TEXT, expire_at NUMERIC)"
            )
            await con.execute(
                "CREATE TABLE IF NOT EXISTS twitchsubscriptions(user_id TEXT PRIMARY KEY, id TEXT)"
            )
            await con.execute(
                "CREATE TABLE IF NOT EXISTS twitchliveconfig(guild_id BIGINT, "
                "channel_id BIGINT, twitch_user_id TEXT, webhook_id BIGINT, template TEXT)"
            )
        for func in [self._schedule_refresh_youtube_subscriptions, self._schedule_refresh_twitch_token]:
            task = self.bot.loop.create_task(func())
            task.add_done_callback(self._handle_task_result)
            self._tasks.append(task)

    async def _schedule_refresh_youtube_subscriptions(self) -> None:
        await self.bot.wait_until_ready()
        while True:
            now = datetime.datetime.now()
            delta = (24 - now.hour - 1) * 60 ** 2 + (60 - now.minute - 1) * 60 + (60 - now.second)
            async with self.bot.db.acquire() as con:
                channel_ids = await con.fetch(
                    f"SELECT channel_id FROM ytsub"
                )
            for entry in channel_ids:
                task = self.bot.loop.create_task(self._wrap_youtube_resubscribe(entry["channel_id"]))
                task.add_done_callback(self._handle_task_result)
                self._tasks.append(task)
            await asyncio.sleep(delta)

    async def _wrap_youtube_resubscribe(self, channel_id: str) -> None:
        return await self._send_youtube_subscribe(channel_id, subscribe=True, force=True)

    async def _schedule_refresh_twitch_token(self) -> None:
        await self.bot.wait_until_ready()
        while True:
            now = datetime.datetime.now()
            delta = (24 - now.hour - 1) * 60 ** 2 + (60 - now.minute - 1) * 60 + (60 - now.second)
            async with self.bot.db.acquire() as con:
                token_data = await con.fetch(
                    f"SELECT * FROM twitchaccess ORDER BY expire_at DESC LIMIT 1"
                )
                if not token_data:
                    token = await self._request_twitch_token()
                else:
                    data = token_data[0]
                    token = TokenData(token=data["token"], token_type=data["type"], expire_at=data["expire_at"])
            if int(now.timestamp()) + 86400 >= token.expire_at:
                delay = delta - 60
                task = self.bot.loop.create_task(self._wrap_twitch_token(delay))
                task.add_done_callback(self._handle_task_result)
                self._tasks.append(task)
            else:
                self._twitch_token = token
            await asyncio.sleep(delta)

    async def _request_twitch_token(self) -> TokenData:
        resp = await self.client_session.post(
            f"https://id.twitch.tv/oauth2/token?client_id={_TWITCH_CID}&client_secret={_TWITCH_CS}&grant_type=client_credentials"
        )
        resp.raise_for_status()
        data = await resp.json()
        now = int(datetime.datetime.now().timestamp())
        token = data["access_token"]
        token_type = data["token_type"]
        expire_at = int(now + data["expires_in"])
        async with self.bot.db.acquire() as con:
            await con.execute(
                "INSERT INTO twitchaccess(token, type, expire_at) VALUES "
                f"($text${token}$text$, $type${token_type}$type$, {expire_at}) "
                "ON CONFLICT (token) DO NOTHING"
            )
        return TokenData(token=token, token_type=token_type, expire_at=expire_at)

    async def _wrap_twitch_token(self, delay: int) -> None:
        await asyncio.sleep(delay)
        token = await self._request_twitch_token()
        self._twitch_token = token

    @staticmethod
    def _handle_task_result(task: asyncio.Task) -> None:
        try:
            task.result()
        except asyncio.CancelledError:
            pass

    async def handle_yt_pub(self, request: web.Request) -> None:
        data = await request.read()
        xml = BeautifulSoup(markup=data.decode(), features="lxml-xml")
        try:
            entry = xml.find(name="entry", recursive=True)
            video_id_entry = entry.find(name="videoId")
            if video_id_entry is None:
                unparsed_video_id = entry.find(name="id")
                if unparsed_video_id is None:
                    raise AttributeError("Cannot find a video ID after checking yt:videoId and id")
                split_ids = unparsed_video_id.text.split(":")
                video_id: str = split_ids[-1]
            else:
                video_id: str = video_id_entry.text
        except AttributeError:
            print("[YOUTUBE PUB]: Unable to find a video ID")
            return web.Response(status=500)
        async with self.bot.db.acquire() as con:
            exists = await con.fetchval(
                f"SELECT video_id FROM ytsubvideoids WHERE video_id=$text${video_id}$text$"
            )
            if exists:
                print("[YOUTUBE PUB]: The video ID has been received before")
                return web.Response(status=200)
            else:
                await con.execute(
                    f"INSERT INTO ytsubvideoids(video_id) VALUES ($text${video_id}$text$) ON CONFLICT "
                    "(video_id) DO NOTHING"
                )
            try:
                video_title: str = entry.find(name="title").text
                channel_id: str = entry.find(name="channelId").text
                _author: str = entry.find(name="author")
                author_name: str = _author.find(name="name").text
                channel_url: str = _author.find(name="uri").text
                video_url_entry = entry.find(name="link", attrs={"rel": "alternate"})
                if video_url_entry is None:
                    video_url = f"https://youtube.com/watch?v={video_id}"
                else:
                    video_url = video_url_entry["href"]
            except AttributeError as e:
                await con.execute(
                    f"DELETE FROM ytsubvideoids WHERE video_id=$text${video_id}$text$"
                )
                print(
                    f"[YOUTUBE PUB]: AttributeError when gathering video metadata: {traceback.format_exc()} (Line: {e.__traceback__.tb_lineno})"
                )
                return web.Response(status=500)
            entries = await con.fetch(
                f"SELECT * from ytsubconfig WHERE yt_channel_id=$text${channel_id}$text$"
            )
            for entry in entries:
                try:
                    channel: discord.TextChannel = self.bot.get_channel(int(entry["channel_id"]))
                    webhook_id = int(entry["webhook_id"])
                    webhook: discord.Webhook = discord.utils.get(await channel.webhooks(), id=webhook_id)
                    if not webhook:
                        await con.execute(
                            f"DELETE FROM ytsubconfig WHERE webhook_id={webhook_id}"
                        )
                    else:
                        await webhook.send(
                            content=str(entry["template"]).replace(
                                "{{author}}", author_name
                            ).replace(
                                "{{title}}", video_title
                            ).replace(
                                "{{url}}", video_url
                            ).replace(
                                "{{channel_url}}", channel_url
                            )
                        )
                except (discord.NotFound, discord.Forbidden, discord.HTTPException):
                    print(
                        f"[YOUTUBE PUB]: Unable to send the webhook message for channel ID: {channel.id}, webhook ID: {webhook.id}"
                    )
        return web.Response(status=200)

    async def handle_twitch_pub(self, request: web.Request) -> None:
        data = await request.json()
        subscription = data.get("subscription")
        event = data.get("event")
        if not (subscription and event):
            return web.Response(status=404)
        if not (subscription["type"] == "stream.online" and subscription["status"] == "enabled"):
            return web.Response(status=404)
        user_id = event["broadcaster_user_id"]
        async with self.bot.db.acquire() as con:
            entries = await con.fetch(
                f"SELECT * FROM twitchliveconfig WHERE twitch_user_id=$text${user_id}$text$"
            )
            if not entries:
                await self._send_twitch_subscribe(user_id, subscribe=False)
            stream = await self._get_twitch_stream(user_id)
            if not stream:
                return web.Response(status=404)
            for entry in entries:
                try:
                    channel_id = entry["channel_id"]
                    channel: discord.TextChannel = self.bot.get_channel(channel_id)
                    webhook_id = int(entry["webhook_id"])
                    webhook: discord.Webhook = discord.utils.get(await channel.webhooks(), id=webhook_id)
                    if not webhook:
                        await con.execute(
                            f"DELETE FROM twitchliveconfig WHERE webhook_id={webhook_id}"
                        )
                    else:
                        await webhook.send(
                            content=str(entry["template"]).replace(
                                "{{author}}", stream.display_name
                            ).replace(
                                "{{title}}", stream.title
                            ).replace(
                                "{{game}}", stream.game,
                            ).replace(
                                "{{url}}", stream.url,
                            )
                        )
                except (discord.NotFound, discord.Forbidden, discord.HTTPException):
                    pass
        return web.Response(status=200)

    async def _get_twitch_user(self, user_id: str = None, username: str = None) -> Union[TwitchUser, None]:
        headers = {
            "Authorization": f"{self._twitch_token.token_type.title()} {self._twitch_token.token}",
            "Client-Id": _TWITCH_CID,
        }
        param = f"id={user_id}" if user_id else f"login={username}"
        resp = await self.client_session.get(_TWITCH_BASE.format(f"users?{param}"), headers=headers)
        resp.raise_for_status()
        payload = await resp.json()
        try:
            data = payload["data"][0]
            return TwitchUser(**data)
        except KeyError:
            return None

    async def _get_twitch_stream(self, user_id: str) -> Union[TwitchStream, None]:
        headers = {
            "Authorization": f"{self._twitch_token.token_type.title()} {self._twitch_token.token}",
            "Client-Id": _TWITCH_CID,
        }
        resp = await self.client_session.get(_TWITCH_BASE.format(f"streams?user_id={user_id}"), headers=headers)
        resp.raise_for_status()
        payload = await resp.json()
        try:
            data = payload["data"][0]
        except KeyError:
            return None
        user = await self._get_twitch_user(user_id=user_id)
        if not user:
            return None
        return TwitchStream(
            game=data["game_name"],
            title=data["title"],
            display_name=user.display_name,
            url=user.stream_url,
            thumbnail_url=user.profile_image_url
        )

    async def _send_youtube_subscribe(self, channel_id: str, subscribe: bool = True, force: bool = False) -> None:
        async with self.bot.db.acquire() as con:
            _cid = await con.fetchval(
                f"SELECT channel_id FROM ytsub WHERE channel_id=$text${channel_id}$text$"
            )
            if force or (_cid and not subscribe) or (not _cid and subscribe):
                expire_at = int(datetime.datetime.now().timestamp()) + (60 * 60 * 24)
                payload = {
                    "hub.callback": os.getenv("PUBSUBCALLBACK"),
                    "hub.mode": f"{'un' if not subscribe else ''}subscribe",
                    "hub.topic": _BASE.format(channel_id),
                    "hub.lease_seconds": 60 * 60 * 24,
                }
                resp = await self.client_session.post(_SUB, data=payload)
                resp.raise_for_status()
                if not subscribe:
                    rely = await con.fetchval(
                        f"SELECT guild_id FROM ytsubconfig WHERE yt_channel_id=$text${channel_id}$text$ LIMIT 1"
                    )
                    if not rely:
                        await con.execute(
                            f"DELETE FROM ytsub WHERE channel_id=$text${channel_id}$text$"
                        )
                else:
                    await con.execute(
                        f"INSERT INTO ytsub(channel_id, expire_at) VALUES($text${channel_id}$text$, {expire_at}) "
                        "ON CONFLICT (channel_id) DO UPDATE SET expire_at=EXCLUDED.expire_at "
                        "WHERE ytsub.channel_id=EXCLUDED.channel_id"
                    )
        return

    async def _send_twitch_subscribe(self, user_id: str, subscribe: bool = True, force: bool = False) -> None:
        async with self.bot.db.acquire() as con:
            entry = await con.fetch(
                f"SELECT * FROM twitchsubscriptions WHERE user_id=$text${user_id}$text$"
            )
            if force or (entry and not subscribe) or (not entry and subscribe):
                if not subscribe:
                    rely = await con.fetchval(
                        f"SELECT guild_id from twitchliveconfig WHERE twitch_user_id=$text${user_id}$text$ LIMIT 1"
                    )
                    if not rely:
                        await self._twitch_unsubscribe(user_id)
                elif subscribe and not entry:
                    await self._twitch_subscribe(user_id)
        return

    async def _twitch_subscribe(self, user_id: str) -> None:
        headers = {
            "Authorization": f"{self._twitch_token.token_type.title()} {self._twitch_token.token}",
            "Client-ID": _TWITCH_CID,
            "Content-Type": "application/json",
        }
        payload = {
            "type": "stream.online",
            "version": "1",
            "condition": {
                "broadcaster_user_id": user_id,
            },
            "transport": {
                "method": "webhook",
                "callback": _TWITCH_CALLBACK,
                "secret": _TWITCH_SECRET,
            }
        }
        resp = await self.client_session.post(
            _TWITCH_BASE.format(f"eventsub/subscriptions"),
            headers=headers,
            json=payload,
        )
        print(await resp.json(), resp.status)
        resp.raise_for_status()
        data = await resp.json()
        _id = data["data"][0]["id"]
        async with self.bot.db.acquire() as con:
            await con.execute(
                "INSERT INTO twitchsubscriptions(user_id, id) VALUES "
                f"($text${user_id}$text$, $text${_id}$text$) ON CONFLICT (user_id) DO NOTHING"
            )
        return

    async def _twitch_unsubscribe(self, user_id: str) -> None:
        headers = {
            "Authorization": f"{self._twitch_token.token_type.title()} {self._twitch_token.token}",
            "Client-ID": _TWITCH_CID,
        }
        async with self.bot.db.acquire() as con:
            sub_id = await con.fetchval(
                f"SELECT id FROM twitchsubscriptions WHERE user_id=$text${user_id}$text$"
            )
            resp = await self.client_session.delete(
                _TWITCH_BASE.format(f"eventsub/subscriptions?id={sub_id}"),
                headers=headers,
            )
            resp.raise_for_status()
            await con.execute(
                f"DELETE FROM twitchsubscriptions WHERE user_id=$text${user_id}$text$"
            )
        return

    async def _guild_subscribe(self, ctx: Context, channel: discord.TextChannel, yt_channel: YouTubeChannel) -> discord.Message:
        async with self.bot.db.acquire() as con:
            exists = await con.fetchval(
                f"SELECT webhook_id FROM ytsubconfig WHERE guild_id={ctx.guild.id} AND "
                f"yt_channel_id=$text${yt_channel.id}$text$"
            )
            if exists:
                return await ctx.channel.send(
                    embed=discord.Embed(
                        title="Notifier Already Exists",
                        description=f"{ctx.author.mention}, a notifier for the channel {yt_channel.title} "
                        f"already exists in {channel.mention}",
                        color=discord.Color.dark_red(),
                    )
                )

            webhook = await self._safely_create_webhook(
                ctx,
                channel,
                name="MarwynnBot YouTube Notifier",
            )

            sp = SetupPanel(
                bot=self.bot,
                ctx=ctx,
                title="YouTube Notifier",
            ).add_step(
                name="description",
                embed=discord.Embed(
                    title="Notification Content",
                    description=f"{ctx.author.mention}, you may customise your notification message here. {_YT_TS}",
                    color=discord.Color.blue(),
                ).set_footer(
                    text=_FC.format("notifier creation")
                ),
                timeout=300,
                break_check=_BC,
            )
            res = await sp.start()
            if res:
                await self._send_youtube_subscribe(yt_channel.id)
                await con.execute(
                    "INSERT INTO ytsubconfig(guild_id, channel_id, yt_channel_id, webhook_id, template) "
                    f"VALUES ({ctx.guild.id}, {channel.id}, $text${yt_channel.id}$text$, "
                    f"{webhook.id}, $text${res[0]}$text$)"
                )
                return await ctx.channel.send(
                    embed=discord.Embed(
                        title="Notifier Successfully Created",
                        description=f"{ctx.author.mention}, a notifier has been set in {channel.mention} "
                        f"for whenever the YouTube channel [{yt_channel.title}]"
                        f"(https://youtube.com/channel/{yt_channel.id}) uploads a new video",
                        color=discord.Color.blue(),
                    ).set_thumbnail(
                        url=yt_channel.thumbnail_url,
                    )
                )

    async def _guild_unsubscribe(self, ctx: Context, channel: discord.TextChannel, yt_channel: YouTubeChannel) -> discord.Message:
        message: discord.Message = await ctx.channel.send(
            embed=discord.Embed(
                title="Confirmation",
                description=f"{ctx.author.mention}, to no longer receive upload notifications for {yt_channel.title}, "
                f"react with {_CONF[0]}, otherwise react with {_CONF[1]} to cancel",
                color=discord.Color.blue(),
            )
        )
        for reaction in _CONF:
            await message.add_reaction(reaction)
        try:
            reaction, _ = await self.bot.wait_for(
                "reaction_add",
                check=lambda r, u: str(r.emoji) in _CONF and r.message.id == message.id and u.id == ctx.author.id,
                timeout=60,
            )
        except asyncio.TimeoutError:
            return await ctx.channel.send(
                embed=discord.Embed(
                    title="Confirmation Timed Out",
                    description=f"{ctx.author.mention}, your notification confirmation timed "
                    "out after 60 seconds of inactivity",
                    color=discord.Color.dark_red(),
                )
            )
        if str(reaction.emoji) == _CONF[0]:
            async with self.bot.db.acquire() as con:
                await con.execute(
                    f"DELETE FROM ytsubconfig WHERE channel_id={channel.id} AND yt_channel_id=$text${yt_channel.id}$text$"
                )
            await self._send_youtube_subscribe(yt_channel.id, subscribe=False)
            embed = discord.Embed(
                title="YouTube Upload Notifier Unsubscribed",
                description=f"{ctx.author.mention}, you will no longer receive upload notifications for "
                f"the YouTube channel {yt_channel.title} in {channel.mention}",
                color=discord.Color.blue(),
            )
        else:
            embed = discord.Embed(
                title="Confirmation Canceled",
                description=f"{ctx.author.mention}, you will keep receiving upload notifications for "
                f"the YouTube channel {yt_channel.title} in {channel.mention}",
                color=discord.Color.blue(),
            )
        return await ctx.channel.send(embed=embed)

    async def _guild_twitch_subscribe(self, ctx: Context, channel: discord.TextChannel, user: TwitchUser) -> discord.Message:
        async with self.bot.db.acquire() as con:
            exists = await con.fetchval(
                f"SELECT guild_id FROM twitchliveconfig WHERE channel_id={channel.id} AND "
                f"twitch_user_id=$text${user.id}$text$"
            )
            if exists:
                return await ctx.channel.send(
                    embed=discord.Embed(
                        title="Notifier Already Exists",
                        description=f"{ctx.author.mention}, a notifier for the user {user.display_name} "
                        f"already exists in {channel.mention}",
                        color=discord.Color.dark_red(),
                    )
                )

            webhook = await self._safely_create_webhook(
                ctx,
                channel,
                name="MarwynnBot Twitch Notifier",
            )

            sp = SetupPanel(
                bot=self.bot,
                ctx=ctx,
                title="Twitch Notifier",
            ).add_step(
                name="description",
                embed=discord.Embed(
                    title="Notification Content",
                    description=f"{ctx.author.mention}, you may customise your notification message here. {_TWITCH_TS}",
                    color=discord.Color.blue(),
                ).set_footer(
                    text=_FC.format("notifier creation")
                ),
                timeout=300,
                break_check=_BC,
            )
            res = await sp.start()
            if res:
                await self._send_twitch_subscribe(user.id)
                await con.execute(
                    "INSERT INTO twitchliveconfig(guild_id, channel_id, twitch_user_id, webhook_id, template) "
                    f"VALUES ({ctx.guild.id}, {channel.id}, $text${user.id}$text$, "
                    f"{webhook.id}, $text${res[0]}$text$)"
                )
                return await ctx.channel.send(
                    embed=discord.Embed(
                        title="Notifier Successfully Created",
                        description=f"{ctx.author.mention}, a notifier has been set in {channel.mention} "
                        f"for whenever the Twitch [{user.display_name}]({user.stream_url}) goes live",
                        color=discord.Color.blue(),
                    ).set_thumbnail(
                        url=user.profile_image_url,
                    )
                )

    async def _guild_twitch_unsubscribe(self, ctx: Context, channel: discord.TextChannel, user: TwitchUser) -> discord.Message:
        message: discord.Message = await ctx.channel.send(
            embed=discord.Embed(
                title="Confirmation",
                description=f"{ctx.author.mention}, to no longer receive twitch stream notifications for {user.display_name}, "
                f"react with {_CONF[0]}, otherwise react with {_CONF[1]} to cancel",
                color=discord.Color.blue(),
            )
        )
        for reaction in _CONF:
            await message.add_reaction(reaction)
        try:
            reaction, _ = await self.bot.wait_for(
                "reaction_add",
                check=lambda r, u: str(r.emoji) in _CONF and r.message.id == message.id and u.id == ctx.author.id,
                timeout=60,
            )
        except asyncio.TimeoutError:
            return await ctx.channel.send(
                embed=discord.Embed(
                    title="Confirmation Timed Out",
                    description=f"{ctx.author.mention}, your notification confirmation timed "
                    "out after 60 seconds of inactivity",
                    color=discord.Color.dark_red(),
                )
            )
        if str(reaction.emoji) == _CONF[0]:
            async with self.bot.db.acquire() as con:
                await con.execute(
                    f"DELETE FROM twitchliveconfig WHERE channel_id={channel.id} AND twitch_user_id=$text${user.id}$text$"
                )
            await self._send_twitch_subscribe(user.id, subscribe=False)
            embed = discord.Embed(
                title="Twitch Stream Notifier Unsubscribed",
                description=f"{ctx.author.mention}, you will no longer receive twitch stream notifications for "
                f"the Twitch {user.display_name} in {channel.mention}",
                color=discord.Color.blue(),
            )
        else:
            embed = discord.Embed(
                title="Confirmation Canceled",
                description=f"{ctx.author.mention}, you will keep receiving twitch stream notifications for "
                f"the Twitch {user.display_name} in {channel.mention}",
                color=discord.Color.blue(),
            )
        return await ctx.channel.send(embed=embed)

    async def _convert_url_to_channel(self, query: str) -> Union[YouTubeChannel, None]:
        if "youtube" in query:
            query_url = yarl.URL(query)
            url = yarl.URL().build(
                scheme="https",
                host="www.googleapis.com",
                path="/youtube/v3/channels",
                query={
                    "part": "snippet",
                    "id": query_url.path.split("/")[-1],
                    "key": _YTAPI,
                }
            )
            resp = await self.client_session.get(url)
            resp.raise_for_status()
            data = await resp.read()
            payload = json.loads(data)
            if payload.get("items"):
                entry = payload["items"][0]
                snippet = entry["snippet"]
                return YouTubeChannel(entry["id"], snippet["title"], snippet["thumbnails"]["high"]["url"])
        return None

    async def _list_yt_notifiers(self, ctx: Context, channel: discord.TextChannel) -> discord.Message:
        embed = discord.Embed(
            title="No YouTube Upload Notifiers",
            description=f"{ctx.author.mention}, there are no notifiers set in {channel.mention}",
            color=discord.Color.blue(),
        )
        async with self.bot.db.acquire() as con:
            channel_ids = await con.fetch(
                f"SELECT yt_channel_id, template FROM ytsubconfig WHERE channel_id={channel.id}"
            )
        if not channel_ids:
            return await ctx.channel.send(embed=embed)
        entries = []
        for entry in channel_ids:
            yt_channel = await self._convert_url_to_channel(f"https://www.youtube.com/channel/{entry['yt_channel_id']}")
            if not yt_channel:
                continue
            entries.append(
                "\n".join([
                    f"**[{yt_channel.title}](https://www.youtube.com/channel/{yt_channel.id})**",
                    f"> Message: ```{entry['template']}```",
                ])
            )
        if not entries:
            return await ctx.channel.send(embed=embed)
        return await EmbedPaginator(
            ctx,
            entries=entries,
            embed=discord.Embed(
                title="YouTube Upload Notifiers",
                color=discord.Color.blue(),
            ),
            description=f"{_YT_TS}\n\n{ctx.author.mention}, here are all upload notifiers in {channel.mention}:\n\n",
        ).paginate()

    async def _edit_yt_notifier(self, ctx: Context, channel: discord.TextChannel, yt_channel: YouTubeChannel) -> discord.Message:
        sp = SetupPanel(
            bot=self.bot,
            ctx=ctx,
            title="YouTube Notifier Edit",
        ).add_step(
            name="description",
            embed=discord.Embed(
                title="Notification Content",
                description=f"{ctx.author.mention}, you may customise your notification message here. " + _YT_TS,
                color=discord.Color.blue(),
            ).set_footer(
                text=_FC.format("notifier edit")
            ),
            timeout=300,
            break_check=_BC,
        )
        res = await sp.start()
        if res:
            async with self.bot.db.acquire() as con:
                await con.execute(
                    f"UPDATE ytsubconfig SET template=$text${res[0]}$text$ WHERE "
                    f"channel_id={channel.id} AND yt_channel_id=$text${yt_channel.id}$text$"
                )
            return await ctx.channel.send(
                embed=discord.Embed(
                    title="Notifier Successfully Updated",
                    description=f"{ctx.author.mention}, the notifier for {yt_channel.title} has been updated",
                    color=discord.Color.blue(),
                ).set_thumbnail(
                    url=yt_channel.thumbnail_url,
                )
            )

    async def _convert_url_to_twitch_user(self, url: str) -> Union[TwitchUser, None]:
        _url = yarl.URL(url)
        username = _url.path[1:]
        user = await self._get_twitch_user(username=username)
        if not user:
            return None
        return user

    async def _list_twitch_notifiers(self, ctx: Context, channel: discord.TextChannel) -> discord.Message:
        embed = discord.Embed(
            title="No Twitch Stream Notifiers",
            description=f"{ctx.author.mention}, there are no notifiers set in {channel.mention}",
            color=discord.Color.blue(),
        )
        async with self.bot.db.acquire() as con:
            user_ids = await con.fetch(
                f"SELECT twitch_user_id, template FROM twitchliveconfig WHERE channel_id={channel.id}"
            )
        if not user_ids:
            return await ctx.channel.send(embed=embed)
        entries = []
        for entry in user_ids:
            twitch_user = await self._get_twitch_user(user_id=entry["twitch_user_id"])
            if not twitch_user:
                continue
            entries.append(
                "\n".join([
                    f"**[{twitch_user.display_name}]({twitch_user.stream_url})**",
                    f"> Message: ```{entry['template']}```",
                ])
            )
        if not entries:
            return await ctx.channel.send(embed=embed)
        return await EmbedPaginator(
            ctx,
            entries=entries,
            embed=discord.Embed(
                title="Twitch Stream Notifiers",
                color=discord.Color.blue(),
            ),
            description=f"{_TWITCH_TS}\n\n{ctx.author.mention}, here are all twitch stream notifiers in {channel.mention}:\n\n",
        ).paginate()

    async def _edit_twitch_notifier(self, ctx: Context, channel: discord.TextChannel, user: TwitchUser) -> discord.Message:
        sp = SetupPanel(
            bot=self.bot,
            ctx=ctx,
            title="Twitch Stream Notifier Edit",
        ).add_step(
            name="description",
            embed=discord.Embed(
                title="Notification Content",
                description=f"{ctx.author.mention}, you may customise your notification message here. " + _TWITCH_TS,
                color=discord.Color.blue(),
            ).set_footer(
                text=_FC.format("notifier edit")
            ),
            timeout=300,
            break_check=_BC,
        )
        res = await sp.start()
        if res:
            async with self.bot.db.acquire() as con:
                await con.execute(
                    f"UPDATE twitchliveconfig SET template=$text${res[0]}$text$ WHERE "
                    f"channel_id={channel.id} AND twitch_user_id=$text${user.id}$text$"
                )
            return await ctx.channel.send(
                embed=discord.Embed(
                    title="Notifier Successfully Updated",
                    description=f"{ctx.author.mention}, the notifier for {user.display_name} has been updated",
                    color=discord.Color.blue(),
                ).set_thumbnail(
                    url=user.profile_image_url,
                )
            )

    async def _safely_create_webhook(self, ctx: Context, channel: discord.TextChannel, name: str) -> discord.Webhook:
        webhook = discord.utils.find(lambda w: w.name == name, await channel.webhooks())
        if webhook == None:
            try:
                webhook = await channel.create_webhook(name=name, avatar=await self.bot.user.avatar_url_as().read())
            except discord.HTTPException:
                await ctx.channel.send(
                    embed=discord.Embed(
                        title="Too Many Webhooks",
                        description=f"{ctx.author.mention}, this channel cannot have more than 10 webhooks at a time",
                        color=discord.Color.dark_red(),
                    ),
                )
                raise SilentButDeadly()
            except discord.Forbidden:
                await ctx.channel.send(
                    embed=discord.Embed(
                        title="Unable to Create Webhook",
                        description=f"{ctx.author.mention}, I cannot create webhooks in {channel.mention}",
                        color=discord.Color.dark_red(),
                    )
                )
                raise SilentButDeadly()
        return webhook

    @commands.command(aliases=["ytnotif"],
                      desc="Sets a notification for every time a channel uploads",
                      usage="youtubenotif (#channel) [youtube_channel_url]",
                      uperms=_MG,
                      note="`(#channel)` is the tag of the text channel you would "
                      "like this notification to be posted in. If `(#channel)` is unspecified, "
                      "it defaults to the current channel")
    @commands.has_permissions(manage_guild=True)
    @raise_handler()
    async def youtubenotif(self, ctx: Context, channel: Optional[discord.TextChannel], *, yt_channel_url: str):
        yt_channel = await self._convert_url_to_channel(yt_channel_url)
        if not yt_channel:
            return await ctx.channel.send(
                embed=discord.Embed(
                    title="No Channel Found",
                    description=f"{ctx.author.mention}, no channel was found "
                    f"with the url `{yt_channel_url}`"
                )
            )
        if not channel:
            channel = ctx.channel
        return await self._guild_subscribe(ctx, channel, yt_channel)

    @commands.command(aliases=["ytunnotif"],
                      desc="Stops notifying for when a channel uploads",
                      usage="youtubeunnotif (#channel) [youtube_channel_url]",
                      uperms=_MG,
                      note="`(#channel)` is the tag of the text channel you would "
                      "like this notification to be posted in. If `(#channel)` is unspecified, "
                      "it defaults to the current channel")
    @commands.has_permissions(manage_guild=True)
    @raise_handler()
    async def youtubeunnotif(self, ctx: Context, channel: Optional[discord.TextChannel], *, yt_channel_url: str):
        yt_channel = await self._convert_url_to_channel(yt_channel_url)
        if not yt_channel:
            return await ctx.channel.send(
                embed=discord.Embed(
                    title="No Channel Found",
                    description=f"{ctx.author.mention}, no channel was found "
                    f"with the url `{yt_channel_url}`"
                )
            )
        if not channel:
            channel = ctx.channel
        return await self._guild_unsubscribe(ctx, channel, yt_channel)

    @commands.command(aliases=["ytnotifedit"],
                      desc="Edits a currently set YouTube upload notifier's message",
                      usage="youtubenotifedit (#channel) [youtube_channel_url]",
                      uperms=_MG,
                      note="`(#channel)` is the tag of the text channel you would "
                      "like to search for a notifier for `[youtube_channel_url]`. If "
                      "`(#channel)` is unspecified, it defaults to the current channel")
    @commands.has_permissions(manage_guild=True)
    @raise_handler()
    async def youtubenotifedit(self, ctx: Context, channel: Optional[discord.TextChannel], *, yt_channel_url: str):
        yt_channel = await self._convert_url_to_channel(yt_channel_url)
        if not yt_channel:
            return await ctx.channel.send(
                embed=discord.Embed(
                    title="No Channel Found",
                    description=f"{ctx.author.mention}, no channel was found "
                    f"with the url `{yt_channel_url}`"
                )
            )
        if not channel:
            channel = ctx.channel
        return await self._edit_yt_notifier(ctx, channel, yt_channel)

    @commands.command(aliases=["ytnotifls"],
                      desc="Lists all upload notifications for the specified channel",
                      usage="youtubenotiflist (#channel)",
                      note="`(#channel)` is the tag of the text channel you would "
                      "like to list all upload notifications for. If `(#channel) is unspecified, "
                      "it defaults to the current channel")
    @raise_handler()
    async def youtubenotiflist(self, ctx: Context, channel: Optional[discord.TextChannel]):
        if not channel:
            channel = ctx.channel
        return await self._list_yt_notifiers(ctx, channel)

    @commands.command(aliases=["tnotif"],
                      desc="Sets a notification for every time a Twitch goes live",
                      usage="twitchnotif (#channel) [twitch_channel_url]",
                      uperms=_MG,
                      note="`(#channel)` is the tag of the text channel you would "
                      "like this notification to be posted in. If `(#channel)` is unspecified, "
                      "it defaults to the current channel")
    @commands.has_permissions(manage_guild=True)
    @raise_handler()
    async def twitchnotif(self, ctx: Context, channel: Optional[discord.TextChannel], *, twitch_channel_url: str):
        twitch_user = await self._convert_url_to_twitch_user(twitch_channel_url)
        if not twitch_user:
            return await ctx.channel.send(
                embed=discord.Embed(
                    title="No User Found",
                    description=f"{ctx.author.mention}, no user was found "
                    f"with the url `{twitch_channel_url}`"
                )
            )
        if not channel:
            channel = ctx.channel
        return await self._guild_twitch_subscribe(ctx, channel, twitch_user)

    @commands.command(aliases=["tunnotif"],
                      desc="Stops notifying for when a Twitch goes live",
                      usage="twitchunnotif (#channel) [twitch_channel_url]",
                      uperms=_MG,
                      note="`(#channel)` is the tag of the text channel you would "
                      "like this notification to be posted in. If `(#channel)` is unspecified, "
                      "it defaults to the current channel")
    @commands.has_permissions(manage_guild=True)
    @raise_handler()
    async def twitchunnotif(self, ctx: Context, channel: Optional[discord.TextChannel], *, twitch_channel_url: str):
        twitch_user = await self._convert_url_to_twitch_user(twitch_channel_url)
        if not twitch_user:
            return await ctx.channel.send(
                embed=discord.Embed(
                    title="No User Found",
                    description=f"{ctx.author.mention}, no user was found "
                    f"with the url `{twitch_channel_url}`"
                )
            )
        if not channel:
            channel = ctx.channel
        return await self._guild_twitch_unsubscribe(ctx, channel, twitch_user)

    @commands.command(aliases=["tnotifedit"],
                      desc="Edits a currently set Twitch stream notifier's message",
                      usage="twitchnotifedit (#channel) [twitch_channel_url]",
                      uperms=_MG,
                      note="`(#channel)` is the tag of the text channel you would "
                      "like to search for a notifier for `[twitch_channel_url]`. If "
                      "`(#channel)` is unspecified, it defaults to the current channel")
    @commands.has_permissions(manage_guild=True)
    @raise_handler()
    async def twitchnotifedit(self, ctx: Context, channel: Optional[discord.TextChannel], *, twitch_channel_url: str):
        twitch_user = await self._convert_url_to_twitch_user(twitch_channel_url)
        if not twitch_user:
            return await ctx.channel.send(
                embed=discord.Embed(
                    title="No User Found",
                    description=f"{ctx.author.mention}, no user was found "
                    f"with the url `{twitch_channel_url}`"
                )
            )
        if not channel:
            channel = ctx.channel
        return await self._edit_twitch_notifier(ctx, channel, twitch_user)

    @commands.command(aliases=["tnotifls"],
                      desc="Lists all Twitch stream notifications for the specified channel",
                      usage="twitchnotiflist (#channel)",
                      note="`(#channel)` is the tag of the text channel you would "
                      "like to list all upload notifications for. If `(#channel) is unspecified, "
                      "it defaults to the current channel")
    @raise_handler()
    async def twitchnotiflist(self, ctx: Context, channel: Optional[discord.TextChannel]):
        if not channel:
            channel = ctx.channel
        return await self._list_twitch_notifiers(ctx, channel)


def setup(bot: AutoShardedBot) -> None:
    bot.add_cog(Notifications(bot))
