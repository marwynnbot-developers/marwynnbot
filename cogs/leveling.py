from collections import namedtuple
from datetime import datetime
from typing import List, Union

import discord
from discord.ext import commands
from utils import (EmbedPaginator, GlobalCMDS, SubcommandHelp,
                   SubcommandPaginator, confirm, customerrors, handle, levels)

lrl = namedtuple("RoleRewardListing", ['level', 'type', 'entries'])


class Leveling(commands.Cog):
    def __init__(self, bot: commands.AutoShardedBot):
        self.bot = bot
        self.gcmds = GlobalCMDS(self.bot)
        self.bot.loop.create_task(self.init_leveling())

    async def init_leveling(self):
        await self.bot.wait_until_ready()
        async with self.bot.db.acquire() as con:
            await con.execute("CREATE TABLE IF NOT EXISTS level_config(guild_id bigint PRIMARY KEY, "
                              "enabled boolean DEFAULT FALSE, route_channel_id bigint DEFAULT NULL, "
                              "freq smallint DEFAULT 1, per_min smallint DEFAULT 20, "
                              "server_notif boolean DEFAULT FALSE, global_notif boolean DEFAULT FALSE)")
            await con.execute("CREATE TABLE IF NOT EXISTS level_disabled(channel_id bigint PRIMARY KEY, guild_id bigint)")
            await con.execute("CREATE TABLE IF NOT EXISTS level_users(user_id bigint, guild_id bigint, "
                              "level smallint DEFAULT 0, xp NUMERIC, last_msg NUMERIC, enabled boolean DEFAULT TRUE)")
            await con.execute("CREATE TABLE IF NOT EXISTS level_global(user_id bigint, level smallint DEFAULT 0, "
                              "xp NUMERIC, last_msg NUMERIC, enabled boolean DEFAULT TRUE)")
            await con.execute("CREATE TABLE IF NOT EXISTS level_roles(role_id bigint PRIMARY KEY, guild_id bigint, "
                              "obtain_at smallint, type text DEFAULT 'add')")
            for guild in self.bot.guilds:
                await con.execute(f"INSERT INTO level_config(guild_id) VALUES({guild.id}) ON CONFLICT DO NOTHING")
        return

    @commands.Cog.listener()
    async def on_guild_join(self, guild: discord.Guild):
        await self.bot.wait_until_ready()
        async with self.bot.db.acquire() as con:
            await con.execute(f"INSERT INTO level_config(guild_id) VALUES({guild.id}) ON CONFLICT DO NOTHING")
        return

    @commands.Cog.listener()
    async def on_guild_remove(self, guild: discord.Guild):
        await self.bot.wait_until_ready()
        async with self.bot.db.acquire() as con:
            for db_name in ['level_config', 'level_roles', 'level_users', 'level_disabled']:
                await con.execute(f"DELETE FROM {db_name} WHERE guild_id={guild.id}")
        return

    @commands.Cog.listener()
    async def on_message(self, message: discord.Message):
        if message.author.bot or not message.guild:
            return
        await levels.calculate_level(self.bot, message)

    async def send_leveling_help(self, ctx) -> discord.Message:
        pfx = f"{await self.gcmds.prefix(ctx)}level"
        description = ("MarwynnBot offers a robust leveling feature that will allow server "
                       "owners to encourage overall engagement with the server. The base "
                       f"command is `{pfx}`. Here are all of MarwynnBot's leveling features")
        return await SubcommandHelp(
            pfx,
            title="Leveling Help",
            description=description
        ).from_config("level").show_help(ctx)

    async def send_leveling_about(self, ctx) -> discord.Message:
        custom_elements = ("- Enabling/Disabling XP gain in certain channels",
                           "- Enabling/Disabling XP gain for certain users",
                           "- Average XP gained per minute",
                           "- How often users can gain XP",
                           "- Directly modify levels and XP for any server member "
                           "*(does not impact global levels and XP)*",
                           "- Routing level up messages to a specific channel",
                           "- Role rewards upon level up",
                           "- Manner in which MarwynnBot adds/removes roles upon level up "
                           "*(eg. upon reaching level x, give user role y. "
                           "Upon reaching lvl n, remove role y, give role z)*")
        description = ("MarwynnBot offers a robust leveling feature that will allow server "
                       "owners to encourage overall engagement with the server. Here are some key elements "
                       "of the leveling system that you'll find to be very useful")
        flexibility = ("MarwynnBot's leveling system has a high degree of flexibility. "
                       "Here is everything server owners can customise per server:\n> " +
                       "\n> ".join(custom_elements))
        rewards = ("MarwynnBot's leveling feature has some built in incentives to encourage activity. "
                   "The leveling leaderboard is available per-server and globally, so MarwynnBot users "
                   "can check their server and global leveling progress and climb the leaderboard. "
                   "Upon reaching specific milestone levels, users will gain a special icon on "
                   "their user profile. Individual servers can also configure roles to reward "
                   "server members upon reaching a certain level, and there are multiple types "
                   "of role rewarding on top of that")
        nv = [("Flexibility", flexibility), ("Rewards", rewards)]
        embed = discord.Embed(title="About Leveling", description=description, color=discord.Color.blue())
        for name, value in nv:
            embed.add_field(name=name, value="> " + "\n> ".join(value), inline=False)
        return await ctx.channel.send(embed=embed)

    async def enable_level(self, ctx, channels: Union[List[discord.TextChannel], None]):
        async with self.bot.db.acquire() as con:
            enabled = await con.fetchval(f"UPDATE level_config SET enabled=TRUE WHERE "
                                         f"enabled=FALSE AND guild_id={ctx.guild.id} RETURNING enabled")
            channel_ids = f"({', '.join([str(channel.id) for channel in channels if channels])})" if channels \
                else f"({', '.join([str(channel.id) for channel in ctx.guild.text_channels])})"
            await con.execute(f"DELETE FROM level_disabled WHERE "
                              f"guild_id={ctx.guild.id} AND channel_id IN {channel_ids}")
        if enabled or not channels:
            description = (f"{ctx.author.mention}, leveling has now been enabled on this server. "
                           "Server members will gain XP whenever they talk, and will level up once they "
                           "pass certain XP thresholds. Configure role rewards with "
                           f"`{await self.gcmds.prefix(ctx)}levelroles`")
        else:
            description = (f"{ctx.author.mention}, leveling was enabled in the following channels:\n" +
                           '\n'.join([channel.mention for channel in channels]))
        embed = discord.Embed(title="Leveling Enabled", description=description, color=discord.Color.blue())
        return await ctx.channel.send(embed=embed)

    @levels.check_entry_exists(entry="enabled", db_name="level_config")
    async def disable_level(self, ctx, channels: Union[List[discord.TextChannel], None]):
        async with self.bot.db.acquire() as con:
            if not channels:
                await con.execute("UPDATE level_config SET enabled=FALSE WHERE "
                                  f"enabled=TRUE AND guild_id={ctx.guild.id}")
                title = "Leveling Disabled"
                description = (f"{ctx.author.mention}, leveling has been disabled on this server. "
                               f"Do `{await self.gcmds.prefix(ctx)}level enable` if you'd like to re-enable it.")
            else:
                values = [f"({channel.id}, {ctx.guild.id})" for channel in channels]
                await con.execute(f"INSERT INTO level_disabled(channel_id, guild_id) "
                                  f"VALUES {', '.join(values)}")
                title = "Leveling Disabled in Specific Channels"
                description = (f"{ctx.author.mention}, leveling has been disabled in the following channels:\n"
                               "\n".join([channel.mention for channel in channels]))
        embed = discord.Embed(title=title, description=description, color=discord.Color.blue())
        return await ctx.channel.send(embed=embed)

    @levels.check_entry_exists(entry="enabled", db_name="level_config")
    async def show_current_xp_rate(self, ctx) -> discord.Message:
        async with self.bot.db.acquire() as con:
            xp_rate = await con.fetchval(f"SELECT per_min FROM level_config WHERE guild_id={ctx.guild.id}")
        embed = discord.Embed(title="XP Rate",
                              description=f"The average XP per minute is {xp_rate} XP per minute for this server",
                              color=discord.Color.blue())
        return await ctx.channel.send(embed=embed)

    @levels.check_entry_exists(entry="enabled", db_name="level_config")
    async def edit_current_xp_rate(self, ctx, value: int) -> discord.Message:
        async with self.bot.db.acquire() as con:
            await con.execute(f"UPDATE level_config SET per_min={value} WHERE guild_id={ctx.guild.id}")
        embed = discord.Embed(title="XP Rate",
                              description="The average XP per minute has been changed to "
                              f"{value} XP per minute for this server",
                              color=discord.Color.blue())
        return await ctx.channel.send(embed=embed)

    @levels.check_entry_exists(entry="enabled", db_name="level_config")
    async def show_current_freq(self, ctx) -> discord.Message:
        async with self.bot.db.acquire() as con:
            freq = await con.fetchval(f"SELECT freq FROM level_config WHERE guild_id={ctx.guild.id}")
        embed = discord.Embed(title="Leveling Frequency",
                              description=f"The frequency of XP gain is once every "
                              f"{freq} {'minutes' if freq != 1 else 'minute'}",
                              color=discord.Color.blue())
        return await ctx.channel.send(embed=embed)

    @levels.check_entry_exists(entry="enabled", db_name="level_config")
    async def edit_current_freq(self, ctx, value: int) -> discord.Message:
        async with self.bot.db.acquire() as con:
            await con.execute(f"UPDATE level_config SET freq={value} WHERE guild_id={ctx.guild.id}")
        embed = discord.Embed(title="Leveling Frequency",
                              description=f"The frequency of XP gain has been changed to once every "
                              f"{value} {'minutes' if value != 1 else 'minute'}",
                              color=discord.Color.blue())
        return await ctx.channel.send(embed=embed)

    @levels.check_entry_exists(entry="enabled", db_name="level_config")
    async def set_notify(self, ctx, mode: str, status: str) -> discord.Message:
        async with self.bot.db.acquire() as con:
            if status in ['f', 'false']:
                curr_status = await con.fetchval(f"UPDATE level_config SET {mode}_notif=FALSE "
                                                 f"WHERE guild_id={ctx.guild.id} RETURNING {mode}_notif")
            elif status in ['t', 'true']:
                curr_status = await con.fetchval(f"UPDATE level_config SET {mode}_notif=TRUE "
                                                 f"WHERE guild_id={ctx.guild.id} RETURNING {mode}_notif")
            else:
                curr_status = await con.fetchval(f"UPDATE level_config SET {mode}_notif=NOT {mode}_notif "
                                                 f"WHERE guild_id={ctx.guild.id} RETURNING {mode}_notif")
        embed = discord.Embed(title="Level Up Notifications Set",
                              description=f"{ctx.author.mention}, notifications for {mode} level ups have been "
                              f"turned {'on' if curr_status else 'off'}",
                              color=discord.Color.blue() if curr_status else discord.Color.dark_red())
        return await ctx.channel.send(embed=embed)

    @levels.check_entry_exists(entry="enabled", db_name="level_config")
    async def configure_reroute(self, ctx, channel: discord.TextChannel = None, name: str = None) -> discord.Message:
        async with self.bot.db.acquire() as con:
            if name == "reroute":
                await con.execute(f"UPDATE level_config SET route_channel_id={channel.id} WHERE guild_id={ctx.guild.id}")
                description = f"The level up messages will now appear in {channel.mention}"
            else:
                await con.execute(f"UPDATE level_config SET route_channel_id=NULL WHERE guild_id={ctx.guild.id}")
                description = ("The level up messages will now appear in "
                               "the channel the user leveled up in")
        embed = discord.Embed(title="Level Up Reroute", description=description, color=discord.Color.blue())
        return await ctx.channel.send(embed=embed)

    async def send_levelroles_help(self, ctx):
        pfx = f"{await self.gcmds.prefix(ctx)}levelroles"
        description = ("Once you have configured MarwynnBot's leveling system, you can configure "
                       f"role rewards for when users level up. The base command is `{pfx}`. "
                       "Here are all the features for configuring role rewards")
        return await SubcommandHelp(
            pfx=pfx,
            title="Levelroles Help",
            description=description
        ).from_config("levelroles").show_help(ctx)

    @levels.check_entry_exists(entry="enabled", db_name="level_config")
    @handle(Exception, to_raise=customerrors.LevelRolesExists())
    async def give_levelroles(self, ctx, level: int, level_type: str, roles: List[discord.Role]) -> discord.Message:
        async with self.bot.db.acquire() as con:
            if isinstance(roles, discord.Role):
                roles = [roles]
            values = [f"({role.id}, {ctx.guild.id}, {level}, '{level_type}')" for role in roles]
            await con.execute(f"INSERT INTO level_roles(role_id, guild_id, obtain_at, type) VALUES"
                              f"{', '.join(values)}")
        description = (f"{ctx.author.mention}, when users on your server reach "
                       f"level {level}, they will receive the following roles:\n" +
                       "\n".join([role.mention for role in roles]))
        if level_type == "add":
            description += "\nThey will be added to previous role rewards they have received"
        else:
            description += "\nThey will be added, but previous role rewards will be removed"
        embed = discord.Embed(title="Successfully Set Role Rewards",
                              description=description,
                              color=discord.Color.blue())
        return await ctx.channel.send(embed=embed)

    @levels.check_entry_exists(entry="enabled", db_name="level_config")
    @handle(Exception, to_raise=customerrors.LevelError())
    async def remove_levelroles(self, ctx, level: int, roles: list) -> discord.Message:
        del_roles = []
        async with self.bot.db.acquire() as con:
            if not roles:
                del_roles = await con.fetch(f"DELETE FROM level_roles WHERE "
                                            f"obtain_at={level} AND guild_id={ctx.guild.id} "
                                            "RETURNING role_id")
            else:
                if isinstance(roles, discord.Role):
                    roles = [roles]
                del_roles = await con.fetch(f"DELETE FROM level_roles WHERE "
                                            f"role_id IN ({', '.join([str(role.id) for role in roles])}) AND "
                                            f"obtain_at={level} RETURNING role_id")
        if not del_roles:
            description = f"{ctx.author.mention}, no role rewards were removed for level {level}"
            color = discord.Color.dark_red()
        else:
            description = (
                f"{ctx.author.mention}, the following role rewards have been "
                f"removed from level {level}:\n" + "\n".join(
                    [f'{ctx.guild.get_role(int(entry["role_id"])).mention}' for entry in del_roles]
                )
            )
            color = discord.Color.blue()
        embed = discord.Embed(title="Successfully Removed Role Reward",
                              description=description,
                              color=color)
        return await ctx.channel.send(embed=embed)

    @levels.check_entry_exists(entry="enabled", db_name="level_config")
    @handle(Exception, to_raise=customerrors.LevelError())
    async def reset_levelroles(self, ctx) -> discord.Message:
        async with self.bot.db.acquire() as con:
            await con.execute(f"DELETE FROM level_roles WHERE guild_id={ctx.guild.id}")
        embed = discord.Embed(title="Level Roles Reset",
                              description=f"{ctx.author.mention}, all role rewards were removed",
                              color=discord.Color.blue())
        return await ctx.channel.send(embed=embed)

    @levels.check_entry_exists(entry="enabled", db_name="level_config")
    @handle(Exception, to_raise=customerrors.LevelError())
    async def list_levelroles(self, ctx, level: int = None) -> discord.Message:
        async with self.bot.db.acquire() as con:
            obtain_type = await con.fetch(f"SELECT DISTINCT(obtain_at), type FROM level_roles "
                                          f"WHERE {f'obtain_at={level} AND ' if level else ''}"
                                          f"guild_id={ctx.guild.id} ORDER BY obtain_at ASC")
            complete_lr = [
                lrl(entry['obtain_at'], entry['type'],
                    await con.fetch(f"SELECT role_id FROM level_roles WHERE "
                                    f"guild_id={ctx.guild.id} AND obtain_at={int(entry['obtain_at'])}"))
                for entry in obtain_type
            ]
        embed = discord.Embed(title=f"Role Rewards{f' for Level {level}' if level else ''}",
                              description=f"Role Rewards for {ctx.guild.name}"
                              f"{f' given at level {level}'if level else ''}",
                              color=discord.Color.blue())
        entries = [
            (f"Level {entry.level}",
             (f"**Role Reward Behavior:** `{entry.type}`", "**Roles Given:**\n> " +
              "\n> ".join(
                  [ctx.guild.get_role(int(record['role_id'])).mention for record in entry.entries])),
             False)
            for entry in complete_lr
        ]
        if not entries:
            embed.description = "No role rewards configured"
            embed.color = discord.Color.dark_red()
            return await ctx.channel.send(embed=embed)
        pag = SubcommandPaginator(ctx, entries=entries, per_page=1, show_entry_count=False, embed=embed)
        return await pag.paginate()

    @levels.check_entry_exists(entry="enabled", db_name="level_config")
    @handle(Exception, to_raise=customerrors.LevelError())
    async def change_lr_level(self, ctx, role: discord.Role, level: int) -> discord.Message:
        async with self.bot.db.acquire() as con:
            level_type = await con.fetchval(f"SELECT DISTINCT(type) FROM level_roles "
                                            f"WHERE obtain_at={level}") or "add"
            await con.execute(f"UPDATE level_roles SET obtain_at={level}, type=$tag${level_type}$tag$ "
                              f"WHERE role_id={role.id}")
        embed = discord.Embed(title="Role Level Changed",
                              description=f"{ctx.author.mention}, server members will now get {role.mention} "
                              f"when they reach level {level}",
                              color=discord.Color.blue())
        return await ctx.channel.send(embed=embed)

    @levels.check_entry_exists(entry="enabled", db_name="level_config")
    @handle(Exception, to_raise=customerrors.LevelError())
    async def change_lr_type(self, ctx, level_type: str, level: int = None):
        async with self.bot.db.acquire() as con:
            if not level:
                await con.execute(f"UPDATE level_roles SET type=$tag${level_type}$tag$ "
                                  f"WHERE guild_id={ctx.guild.id}")
                description = (f"{ctx.author.mention}, all the role rewards in this server "
                               f"were updated to type `{level_type}`")
            else:
                await con.execute(f"UPDATE level_roles SET type=$tag${level_type}$tag$ "
                                  f"WHERE guild_id={ctx.guild.id} AND obtain_at={level}")
                description = (f"{ctx.author.mention}, all the level {level} role rewards "
                               f"in this server were updated to type `{level_type}`")
        embed = discord.Embed(title="Role Reward Type Changed",
                              description=description,
                              color=discord.Color.blue())
        return await ctx.channel.send(embed=embed)

    @handle(Exception, to_raise=customerrors.LevelError())
    async def _get_leaderboard(self, guild: discord.Guild) -> List[str]:
        async with self.bot.db.acquire() as con:
            entries = await con.fetch("SELECT user_id, level, xp FROM level_users WHERE "
                                      f"guild_id={guild.id} ORDER BY level DESC, xp DESC")
        return [f"<@{item['user_id']}> ⟶ Level **{item['level']}** "
                f"*({item['xp']}/{levels._calc_req_xp(int(item['level']))})*" for item in entries]

    @commands.group(invoke_without_command=True,
                    aliases=['lvl', 'levels'],
                    desc="Displays the user's server level",
                    usage="level (subcommand | @member)",
                    note="Do `m!level help` to view the help menu for level subcommands. "
                    "Do `m!levelroles` to view the help menu for levelroles subcommands")
    async def level(self, ctx, *, search: str = ""):
        mode = "guild"
        if search.endswith("-g"):
            search = search[:-3 if search.endswith(" -g") else -2]
            mode = "global"
        try:
            member = await commands.MemberConverter().convert(ctx, search)
        except commands.MemberNotFound:
            member = ctx.author
        export, embed = await levels.gen_profile(self.bot, member, mode=mode)
        embed.set_footer(
            text=f"This is your {mode if mode == 'global' else 'server'} level",
        )
        return await ctx.channel.send(file=export, embed=embed)

    @level.command(aliases=['h', 'help'])
    async def level_help(self, ctx):
        return await self.send_leveling_help(ctx)

    @level.command(aliases=['on', 'enable'])
    @commands.has_permissions(manage_guild=True)
    async def level_enable(self, ctx, *, channels: commands.Greedy[discord.TextChannel] = None):
        if channels:
            if hasattr(channels, "id"):
                channels = [channels]
        return await self.enable_level(ctx, channels)

    @level.command(aliases=['off', 'disable'])
    @commands.has_permissions(manage_guild=True)
    async def level_disable(self, ctx, *, channels: commands.Greedy[discord.TextChannel] = None):
        if channels:
            if hasattr(channels, "id"):
                channels = [channels]
        return await self.disable_level(ctx, channels)

    @level.command(aliases=['xpm', 'xppermin', 'xpperminute'])
    @commands.cooldown(1.0, 60.0, commands.BucketType.guild)
    @commands.has_permissions(manage_guild=True)
    async def level_xpperminute(self, ctx, value: int = None):
        if not value:
            return await self.show_current_xp_rate(ctx)
        else:
            return await self.edit_current_xp_rate(ctx, value)

    @level.command(aliases=['freq', 'rate', 'frequency'])
    @commands.cooldown(1.0, 60.0, commands.BucketType.guild)
    @commands.has_permissions(manage_guild=True)
    async def level_frequency(self, ctx, value: int = None):
        if not value:
            return await self.show_current_freq(ctx)
        else:
            return await self.edit_current_freq(ctx, value)

    @level.command(aliases=['redirect', 'reroute'])
    @commands.has_permissions(manage_guild=True)
    async def level_reroute(self, ctx, channel: discord.TextChannel):
        return await self.configure_reroute(ctx, channel=channel, name="reroute")

    @level.command(aliases=['unredirect', 'unroute'])
    @commands.has_permissions(manage_guild=True)
    async def level_unroute(self, ctx):
        return await self.configure_reroute(ctx, name="reroute")

    @level.command(aliases=['notif', 'notifs', 'notify'])
    @commands.has_permissions(manage_guild=True)
    async def level_notify(self, ctx, mode: str, status: str = "toggle"):
        if mode.lower() in ['s', 'server']:
            mode = 'server'
        elif mode.lower() in ['g', 'global']:
            mode = 'global'
        else:
            raise customerrors.LevelInvalidNotifyMode(mode)
        return await self.set_notify(ctx, mode, status.lower())

    @level.command(aliases=['info', 'about'])
    async def level_about(self, ctx):
        return await self.send_leveling_about(ctx)

    @commands.group(invoke_without_command=True,
                    aliases=['lr', 'lvlrole', 'levelrole'],
                    desc="Displays the help command for levelroles",
                    usage="levelroles (subcommand)",
                    note="You must have leveling set up first. Do `m!level` to see all available "
                    "commands for configuring MarwynnBot's leveling system on your server")
    async def levelroles(self, ctx):
        return await self.send_levelroles_help(ctx)

    @levelroles.command(aliases=['set', 'give'])
    @commands.has_permissions(manage_guild=True)
    async def levelroles_give(self, ctx, level: int, level_type: str, roles: commands.Greedy[discord.Role]):
        if not 1 <= level <= 100:
            raise customerrors.LevelInvalidRange(level)
        if not level_type.lower() in ['add', 'a', 'replace', 'r']:
            raise customerrors.LevelInvalidType(level_type)

        if level_type.lower() == "a":
            level_type = "add"
        elif level_type.lower() == "r":
            level_type = "replace"
        else:
            level_type = level_type.lower()
        return await self.give_levelroles(ctx, level, level_type, roles)

    @levelroles.command(aliases=['unset', 'remove'])
    @commands.has_permissions(manage_guild=True)
    async def levelroles_remove(self, ctx, level: int, roles: commands.Greedy[discord.Role] = None):
        if not 1 <= level <= 100:
            raise customerrors.LevelInvalidRange(level)
        return await confirm(ctx, self.bot,
                             user=ctx.author,
                             success_func=self.remove_levelroles(ctx, level, roles),
                             op="levelroles remove")

    @levelroles.command(aliases=['clear', 'reset'])
    @commands.has_permissions(manage_guild=True)
    async def levelroles_reset(self, ctx):
        return await confirm(ctx, self.bot,
                             user=ctx.author,
                             success_func=self.reset_levelroles(ctx),
                             op="levelroles reset")

    @levelroles.command(aliases=['show', 'ls', 'list'])
    async def levelroles_list(self, ctx, level: int = None):
        if level and not 1 <= level <= 100:
            raise customerrors.LevelInvalidRange(level)
        return await self.list_levelroles(ctx, level)

    @levelroles.command(aliases=['cl', 'levelchange', 'changelevel'])
    @commands.has_permissions(manage_guild=True)
    async def levelroles_changelevel(self, ctx, role: discord.Role, level: int):
        if not 1 <= level <= 100:
            raise customerrors.LevelInvalidRange(level)
        return await confirm(ctx, self.bot,
                             user=ctx.author,
                             success_func=self.change_lr_level(ctx, role, level),
                             op="levelroles change level")

    @levelroles.command(aliases=['ct', 'typechange', 'changetype'])
    @commands.has_permissions(manage_guild=True)
    async def levelroles_changetype(self, ctx, level_type: str, level: int = None):
        if level and not 1 <= level <= 100:
            raise customerrors.LevelInvalidRange(level)

        if not level_type.lower() in ['a', 'add', 'r', 'replace']:
            raise customerrors.LevelInvalidType(level_type)

        if level_type.lower() == "a":
            level_type = "add"
        elif level_type.lower() == "r":
            level_type = "replace"
        else:
            level_type = level_type.lower()
        return await confirm(ctx, self.bot,
                             user=ctx.author,
                             success_func=self.change_lr_type(ctx, level_type, level=level),
                             op="levelroles change type")

    @commands.command(aliases=['lb'],
                      desc="Shows the server leveling and XP leaderboard",
                      usage="leaderboard",
                      note="This only displays members that have a database entry for leveling")
    async def leaderboard(self, ctx):
        entries = await self._get_leaderboard(ctx.guild)
        embed = discord.Embed(title=f"Leaderboard for {ctx.guild.name}", color=discord.Color.blue())
        embed.set_thumbnail(url=ctx.guild.icon_url)
        embed.set_footer(text=f"Current as of {datetime.now().strftime('%m/%d/%Y %H:%M:%S')}")
        description = (f"Here is the leaderboard for {ctx.guild.name}. "
                       "Climb the leaderboard by simply talking in the enabled channels!\n\n")
        pag = EmbedPaginator(ctx,
                             entries=entries,
                             per_page=10,
                             show_entry_count=False,
                             embed=embed,
                             description=description)
        return await pag.paginate()


def setup(bot):
    bot.add_cog(Leveling(bot))
