import asyncio
from typing import List

from discord.ext import commands
from discord.ext.commands import AutoShardedBot


class Status(commands.Cog):
    def __init__(self, bot: AutoShardedBot) -> None:
        self.bot = bot
        self._tasks: List[asyncio.Task] = []
        task = self.bot.loop.create_task(self.report_status())
        task.add_done_callback(self._handle_task_result)
        self._tasks.append(task)

    def cog_unload(self):
        for task in self._tasks:
            task.cancel()

    @staticmethod
    def _handle_task_result(task: asyncio.Task) -> None:
        try:
            task.result()
        except asyncio.CancelledError:
            pass

    async def report_status(self) -> None:
        await self.bot.wait_until_ready()
        async with self.bot.db.acquire() as con:
            await con.execute(
                "CREATE TABLE IF NOT EXISTS status(bot TEXT PRIMARY KEY, core TEXT, "
                "db TEXT, music TEXT, notifications TEXT, serverlink TEXT, token TEXT, api TEXT)"
            )
            await con.execute(
                f"INSERT INTO status(bot) VALUES ('mb') ON CONFLICT (bot) DO NOTHING"
            )
        while True:
            async with self.bot.db.acquire() as con:
                for key, value in self.bot._status.items():
                    await con.execute(
                        f"UPDATE status SET {key}=$text${value}$text$ WHERE bot='mb'"
                    )
                await asyncio.sleep(120)


def setup(bot: AutoShardedBot) -> None:
    bot.add_cog(Status(bot))
