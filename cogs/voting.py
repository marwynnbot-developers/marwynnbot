import json
import os

import discord
from discord.ext import commands, tasks
from utils import GlobalCMDS

TOPGG_URL = "https://top.gg/bot/623317451811061763/vote"
DBL = "https://discordbotlist.com/bots/marwynnbot/upvote"
DXTR = "https://discordextremelist.xyz/en-US/bots/623317451811061763"
DLABS = "https://bots.discordlabs.org/bot/623317451811061763?vote"


class Voting(commands.Cog):
    def __init__(self, bot: commands.AutoShardedBot):
        self.bot = bot
        self.gcmds = GlobalCMDS(self.bot)
        self.bot.loop.create_task(self._init_db())

    async def _init_db(self):
        await self.bot.wait_until_ready()
        async with self.bot.db.acquire() as con:
            await con.execute(f"CREATE TABLE IF NOT EXISTS vote(data TEXT, timestamp NUMERIC)")
        self._process_vote.start()

    def cog_unload(self):
        self._process_vote.cancel()

    @tasks.loop(seconds=30)
    async def _process_vote(self) -> None:
        owner: discord.User = self.bot.get_user(int(os.getenv("OWNER_ID")))
        async with self.bot.db.acquire() as con:
            new_entries = await con.fetch(
                f"DELETE FROM vote RETURNING *;"
            )
        for entry in new_entries:
            data = json.loads(str(entry["data"]))
            user_id = data["user"]
            location = data["source"]
            try:
                user: discord.User = self.bot.get_user(int(user_id))
            except discord.NotFound:
                continue
            _credits = 420 if data.get("isWeekend") else 200
            await self.gcmds.balance_db(
                f"INSERT INTO balance(user_id, amount) VALUES ({user_id}, {1000 + _credits}) ON CONFLICT (user_id) "
                f"DO UPDATE SET amount=balance.amount + {_credits} WHERE balance.user_id=EXCLUDED.user_id"
            )
            embed = discord.Embed(
                title="Thank you for Voting!",
                description=f"{user.mention}, thank you for supporting MarwynnBot on {location}! Your vote helps "
                "me grow and provide my services to more users! As a token of appreciation from MarwynnBot "
                f"developers, we've given you **{_credits} credits** to use for MarwynnBot's various games",
                color=discord.Color.blue(),
            ).set_footer(
                text=f"The dev team is working on giving out better rewards for upvoters! Please stay tuned for more info!"
            )
            await owner.send(embed=discord.Embed(
                title=f"MarwynnBot Upvoted!",
                description=f"I just received an upvote from {user.mention} ({user}) on {location}!",
                color=discord.Color.blue(),
            ).set_thumbnail(
                url=user.avatar_url,
            ))
            try:
                await user.send(embed=embed)
            except Exception:
                pass

    @commands.command(desc="Returns the link to vote for MarwynnBot",
                      usage="vote")
    async def vote(self, ctx):
        description = (f"{ctx.author.mention}, please vote for MarwynnBot "
                       "and show your support by voting on these bot listings:\n",
                       f"1. {TOPGG_URL}",
                       f"2. {DBL}",
                       f"3. {DLABS}")
        embed = discord.Embed(
            title="Vote for MarwynnBot!",
            description="\n> ".join(description),
            color=discord.Color.blue()
        ).set_footer(
            text="As a token of my appreciation, you'll receive 200 credits (420 on weekends) "
            "per vote per listing. Please stay tuned, as better rewards will be added!"
        )
        return await ctx.channel.send(embed=embed)


def setup(bot):
    bot.add_cog(Voting(bot))
