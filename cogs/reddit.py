import random
from datetime import datetime

import asyncpraw
import discord
from asyncpraw.reddit import Redditor
from discord.ext import commands
from utils import GlobalCMDS


class Reddit(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.gcmds = GlobalCMDS(self.bot)

    async def get_id_secret(self, ctx):
        client_id = self.gcmds.env_check("REDDIT_CLIENT_ID")
        client_secret = self.gcmds.env_check("REDDIT_CLIENT_SECRET")
        user_agent = self.gcmds.env_check("USER_AGENT")
        if not all([client_id, client_secret, user_agent]):
            title = "Missing Reddit Client ID or Client Secret or User Agent"
            description = "Insert your Reddit Client ID, Client Secret, and User Agent in the `.env` file"
            embed = discord.Embed(title=title,
                                  description=description,
                                  color=discord.Color.dark_red())
            await ctx.channel.send(embed=embed)
        return client_id, client_secret, user_agent

    async def embed_template(self, ctx):
        client_id, client_secret, user_agent = await self.get_id_secret(ctx)

        if not all([client_id, client_secret, user_agent]):
            return

        reddit = asyncpraw.Reddit(client_id=client_id, client_secret=client_secret, user_agent=user_agent)
        picture_search = await reddit.subreddit(ctx.command.name)

        submissions = []

        async for post in picture_search.hot(limit=100):
            if (not post.stickied and not post.over_18) and not "https://v.redd.it/" in post.url:
                submissions.append(post)

        picture = random.choice(submissions)

        author: Redditor = picture.author
        await author.load()
        real_timestamp = datetime.fromtimestamp(picture.created_utc).strftime("%d/%m/%Y %H:%M:%S")
        embed = discord.Embed(
            title=picture.subreddit_name_prefixed,
            url=f"https://www.reddit.com/{picture.permalink}",
            color=discord.Color.blue()
        ).set_author(
            name=author,
            url=f"https://www.reddit.com/user/{author}/",
            icon_url=author.icon_img
        ).set_image(
            url=picture.url
        ).set_footer(
            text=(f"⬆️{picture.score}️ ({picture.upvote_ratio * 100}%)\n💬{picture.num_comments}\n🕑{real_timestamp}\n"
                  f"Copyrights belong to their respective owners")
        )
        return await ctx.channel.send(embed=embed)

    @commands.command(aliases=['reddithelp', 'rh'],
                      desc="Displays the help command for reddit",
                      usage="reddit (subreddit)",
                      note="Valid subreddit names are listed in the help command")
    async def reddit(self, ctx, cmdName=None):
        CMDNAMES = [command.name for command in self.get_commands() if command.name != "reddit"]
        description = f"Do `{await self.gcmds.prefix(ctx)}reddit [cmdName]` to get the usage of that particular " \
                      f"command.\n\n**List of all {len(CMDNAMES)} reddit commands:**\n\n `{'` `'.join(sorted(CMDNAMES))}` "
        if not cmdName or cmdName == "reddit":
            helpEmbed = discord.Embed(title="Reddit Commands Help",
                                      description=description,
                                      color=discord.Color.blue())
        else:
            if cmdName in CMDNAMES:
                r_command = cmdName.capitalize()
                helpEmbed = discord.Embed(title=f"{r_command}",
                                          description=f"Returns a randomly selected image from the subreddit r/{cmdName}",
                                          color=discord.Color.blue())
                helpEmbed.add_field(name="Usage",
                                    value=f"`{await self.gcmds.prefix(ctx)}{cmdName}`",
                                    inline=False)
                aliases = self.bot.get_command(name=cmdName).aliases
                if aliases:
                    value = "`" + "` `".join(sorted(aliases)) + "`"
                    helpEmbed.add_field(name="Aliases", value=value, inline=False)
            else:
                helpEmbed = discord.Embed(title="Command Not Found",
                                          description=f"{ctx.author.mention}, {cmdName} is not a valid reddit command",
                                          color=discord.Color.blue())
        return await ctx.channel.send(embed=helpEmbed)

    @commands.command(aliases=['abj', 'meananimals'],
                      desc="Gets a picture from the subreddit r/animalsbeingjerks",
                      usage="animalsbeingjerks",)
    async def animalsbeingjerks(self, ctx):
        return await self.embed_template(ctx)

    @commands.command(aliases=['anime'],
                      desc="Gets a picture from the subreddit r/awwnime",
                      usage="awwnime",)
    async def awwnime(self, ctx):
        return await self.embed_template(ctx)

    @commands.command(aliases=['car', 'cars', 'carpics'],
                      desc="Gets a picture from the subreddit r/carporn",
                      usage="carporn",)
    async def carporn(self, ctx):
        return await self.embed_template(ctx)

    @commands.command(desc="Gets a picture from the subreddit r/cosplay",
                      usage="cosplay",)
    async def cosplay(self, ctx):
        return await self.embed_template(ctx)

    @commands.command(aliases=['earth', 'earthpics'],
                      desc="Gets a picture from the subreddit r/earthporn",
                      usage="earthporn",)
    async def earthporn(self, ctx):
        return await self.embed_template(ctx)

    @commands.command(aliases=['food', 'foodpics'],
                      desc="Gets a picture from the subreddit r/foodporn",
                      usage="foodporn",)
    async def foodporn(self, ctx):
        return await self.embed_template(ctx)

    @commands.command(aliases=['animemes'],
                      desc="Gets a picture from the subreddit r/goodanimemes",
                      usage="goodanimemes",)
    async def goodanimemes(self, ctx):
        return await self.embed_template(ctx)

    @commands.command(aliases=['history', 'historypics'],
                      desc="Gets a picture from the subreddit r/historyporn",
                      usage="historyporn",)
    async def historyporn(self, ctx):
        return await self.embed_template(ctx)

    @commands.command(aliases=['pic', 'itap'],
                      desc="Gets a picture from the subreddit r/itookapicture",
                      usage="itookapicture",)
    async def itookapicture(self, ctx):
        return await self.embed_template(ctx)

    @commands.command(aliases=['map', 'maps', 'mappics'],
                      desc="Gets a picture from the subreddit r/mapporn",
                      usage="mapporn",)
    async def mapporn(self, ctx):
        return await self.embed_template(ctx)

    @commands.command(aliases=['interesting', 'mi'],
                      desc="Gets a picture from the subreddit r/mildlyinteresting",
                      usage="mildlyinteresting",)
    async def mildlyinteresting(self, ctx):
        return await self.embed_template(ctx)

    @commands.command(desc="Gets a picture from the subreddit r/pareidolia",
                      usage="pareidolia",)
    async def pareidolia(self, ctx):
        return await self.embed_template(ctx)

    @commands.command(aliases=['ptiming'],
                      desc="Gets a picture from the subreddit r/perfecttiming",
                      usage="perfecttiming",)
    async def perfecttiming(self, ctx):
        return await self.embed_template(ctx)

    @commands.command(aliases=['psbattle'],
                      desc="Gets a picture from the subreddit r/photoshopbattles",
                      usage="photoshopbattles",)
    async def photoshopbattles(self, ctx):
        return await self.embed_template(ctx)

    @commands.command(aliases=['quotes'],
                      desc="Gets a picture from the subreddit r/quotesporn",
                      usage="quotesporn",)
    async def quotesporn(self, ctx):
        return await self.embed_template(ctx)

    @commands.command(aliases=['room', 'rooms', 'roompics'],
                      desc="Gets a picture from the subreddit r/roomporn",
                      usage="roomporn",)
    async def roomporn(self, ctx):
        return await self.embed_template(ctx)

    @commands.command(desc="Gets a picture from the subreddit r/tumblr",
                      usage="tumblr",)
    async def tumblr(self, ctx):
        return await self.embed_template(ctx)

    @commands.command(desc="Gets a picture from the subreddit r/unexpected",
                      usage="unexpected",)
    async def unexpected(self, ctx):
        return await self.embed_template(ctx)

    @commands.command(aliases=['wallpaper'],
                      desc="Gets a picture from the subreddit r/wallpapers",
                      usage="wallpapers",)
    async def wallpapers(self, ctx):
        return await self.embed_template(ctx)

    @commands.command(aliases=['woah'],
                      desc="Gets a picture from the subreddit r/woahdude",
                      usage="woahdude",)
    async def woahdude(self, ctx):
        return await self.embed_template(ctx)


def setup(bot):
    bot.add_cog(Reddit(bot))
