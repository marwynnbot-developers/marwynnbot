import importlib

import discord
import utils
from discord.ext import commands
from discord.ext.commands import AutoShardedBot, Context
from setuppanel import SetupPanel

OWNER_PERM = ["Bot Owner Only"]


class Testing(commands.Cog):
    def __init__(self, bot: AutoShardedBot) -> None:
        self.bot = bot

    @commands.command(desc="Testing purposes",
                      usage="setuptest",
                      uperms=OWNER_PERM,)
    @commands.is_owner()
    async def setuptest(self, ctx: Context):
        sp = SetupPanel(
            bot=self.bot,
            ctx=ctx,
            title="Test Setup Panel",
        ).add_step(
            name="content",
            embed=discord.Embed(
                title="Test Setup",
                description=f"{ctx.author.mention}, message content please",
                color=discord.Color.blue(),
            ),
            timeout=300,
        )
        for name in ["channel", "role", "member"]:
            sp.add_step(
                name=name,
                embed=discord.Embed(
                    title="Test Setup",
                    description=f"{ctx.author.mention}, mention a {name}",
                    color=discord.Color.blue(),
                ),
                timeout=300,
            )
        sp.add_until_finish(
            name="content",
            embed=discord.Embed(
                title="Test Setup",
                description=f"{ctx.author.mention}, message content please",
                color=discord.Color.blue(),
            ),
            timeout=300,
            break_check=lambda m: m.content == "finish" and m.author == ctx.author and m.channel == ctx.channel,
        ).add_conditional_step(
            name="integer",
            embed=discord.Embed(
                title="Test Setup",
                description=f"{ctx.author.mention}, please specify an integer value",
                color=discord.Color.blue(),
            ),
            timeout=300,
            condition=lambda lv: bool(lv)
        )
        res = await sp.start()
        await ctx.channel.send(content=res)

    @commands.command(desc="Reload SetupPanel",
                      usage="setupreload",
                      uperms=OWNER_PERM,)
    @commands.is_owner()
    async def setupreload(self, ctx):
        importlib.reload(utils.setuppanel)
        print("SetupPanel module reloaded")


def setup(bot: AutoShardedBot) -> None:
    bot.add_cog(Testing(bot))
