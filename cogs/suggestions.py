from math import ceil, log

import discord
from discord.ext import commands
from discord.ext.commands import AutoShardedBot, Context
from setuppanel import SetupPanel
from utils import GlobalCMDS, SubcommandHelp

_PASSED_THRESHOLD_BUILTIN = lambda g, c: c >= int(
    ceil(log(len([member for member in g.members if not member.bot]), 4) / 2) or 1)


class Suggestions(commands.Cog):
    def __init__(self, bot: AutoShardedBot) -> None:
        self.bot = bot
        self.gcmds = GlobalCMDS(self.bot)
        self.bot.loop.create_task(self.init_db())

    async def init_db(self) -> None:
        await self.bot.wait_until_ready()
        async with self.bot.db.acquire() as con:
            await con.execute(
                "CREATE TABLE IF NOT EXISTS suggestions(guild_id bigint PRIMARY KEY, channel_id bigint, "
                "emoji TEXT, threshold smallint DEFAULT NULL, enabled BOOLEAN DEFAULT TRUE)"
            )

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload: discord.RawReactionActionEvent) -> None:
        await self.bot.wait_until_ready()
        if payload.member and not payload.member.bot:
            channel = await self.bot.fetch_channel(payload.channel_id)
            async with self.bot.db.acquire() as con:
                emoji = await con.fetchval(f"SELECT emoji FROM suggestions WHERE guild_id={channel.guild.id} AND enabled=TRUE")
                if emoji and str(payload.emoji) == emoji:
                    message: discord.Message = await channel.fetch_message(payload.message_id)
                    channel_id = await con.fetchval(f"SELECT channel_id FROM suggestions WHERE guild_id={channel.guild.id}")
                    if channel_id:
                        channel = await self.bot.fetch_channel(channel_id)
                        threshold = await con.fetchval(f"SELECT threshold FROM suggestions WHERE guild_id={channel.guild.id} AND channel_id={channel.id} AND enabled=TRUE")
                        reaction = [r for r in message.reactions if str(r.emoji) == emoji][0]
                        count = len([user for user in await reaction.users().flatten() if not user.bot])
                        if (threshold and count >= threshold) or _PASSED_THRESHOLD_BUILTIN(channel.guild, count):
                            await self.push(channel, message)

    @staticmethod
    async def push(channel: discord.TextChannel, message: discord.Message):
        return await channel.send(
            content=message.content,
            embed=message.embeds[0] if message.embeds else None,
            files=[await attachment.to_file() for attachment in message.attachments] if message.attachments else None,
        )

    async def show_help(self, ctx: Context) -> discord.Message:
        pfx = f"{await self.gcmds.prefix(ctx)}suggestions"
        return await SubcommandHelp(
            pfx=pfx,
            title="Suggestions Help",
            description="MarwynnBot's suggestions feature provides means of implementing a functioning anonymous suggestions channel "
            "in your server. It is similar to the starboard feature by design, however, it provides some extra options for increased "
            f"flexibility. The base command is {pfx}. Here are all valid subcommands",
            per_page=4,
        ).from_config("suggestions").show_help(ctx)

    @commands.group(invoke_without_command=True,
                    aliases=["sgt", "suggestion"],
                    desc="Displays the help for suggestions subcommands",
                    usage="suggestions (subcommand)")
    async def suggestions(self, ctx: Context):
        return await self.show_help(ctx)

    @suggestions.group(invoke_without_command=True,
                       name="bind",
                       aliases=["register", "set"],)
    async def suggestions_bind(self, ctx: Context):
        return await self.show_help(ctx)

    @suggestions_bind.command(name="channel",)
    @commands.has_permissions(manage_guild=True)
    async def suggestions_bind_channel(self, ctx: Context, channel: discord.TextChannel):
        async with self.bot.db.acquire() as con:
            await con.execute(
                f"INSERT INTO suggestions(guild_id, channel_id) VALUES ({ctx.guild.id}, {channel.id}) ON CONFLICT "
                f"(guild_id) DO UPDATE SET channel_id=EXCLUDED.channel_id WHERE suggestions.guild_id={ctx.guild.id}"
            )
        embed = discord.Embed(
            title="Suggestions Channel Bound",
            description=f"{ctx.author.mention}, the suggestions channel has been successfully bound to {channel.mention}",
            color=discord.Color.blue(),
        )
        return await ctx.channel.send(embed=embed)

    @suggestions_bind.command(name="emoji",)
    @commands.has_permissions(manage_guild=True)
    async def suggestions_bind_emoji(self, ctx: Context):
        ret = await SetupPanel(
            bot=self.bot,
            ctx=ctx,
            title="Suggestions Emoji Bind",
        ).add_step(
            name="emoji",
            embed=discord.Embed(
                title="Suggestions Emoji Setup",
                description=f"{ctx.author.mention}, please react to this message with the emoji you would like to bind to suggestions",
                color=discord.Color.blue(),
            ),
            timeout=60,
        )()
        if ret:
            emoji = ret[0]
            async with self.bot.db.acquire() as con:
                await con.execute(
                    f"INSERT INTO suggestions(guild_id, emoji) VALUES ({ctx.guild.id}, $emoji${str(emoji)}$emoji$)"
                    f"ON CONFLICT (guild_id) DO UPDATE SET emoji=EXCLUDED.emoji WHERE suggestions.guild_id={ctx.guild.id}"
                )
            embed = discord.Embed(
                title="Suggestions Emoji Bound",
                description=f"{ctx.author.mention}, the suggestions emoji has been successfully bound to {str(emoji)}",
                color=discord.Color.blue(),
            )
            return await ctx.channel.send(embed=embed)

    @suggestions.group(invoke_without_command=True,
                       name="unbind",
                       aliases=["unregister", "unset"],)
    async def suggestions_unbind(self, ctx: Context):
        return await self.show_help(ctx)

    @suggestions_unbind.command(name="channel",)
    @commands.has_permissions(manage_guild=True)
    async def suggestions_unbind_channel(self, ctx: Context):
        embed = discord.Embed(
            title="Suggestions Channel Unbound",
            description=f"{ctx.author.mention}, the suggestions channel has been successfully unbound",
            color=discord.Color.blue(),
        )
        async with self.bot.db.acquire() as con:
            entry = await con.fetchval(f"SELECT channel_id FROM suggestions WHERE guild_id={ctx.guild.id}")
            if entry:
                await con.execute(
                    f"UPDATE suggestions SET channel_id=NULL WHERE suggestions.guild_id={ctx.guild.id}"
                )
            else:
                embed.title = "Suggestions Channel Not Bound"
                embed.description = f"{ctx.author.mention}, no channel was previously bound. Cannot unbind nonexistent channel"
                embed.color = discord.Color.dark_red()
        return await ctx.channel.send(embed=embed)

    @suggestions_unbind.command(name="emoji",)
    @commands.has_permissions(manage_guild=True)
    async def suggestions_unbind_emoji(self, ctx: Context):
        embed = discord.Embed(
            title="Suggestions Emoji Unbound",
            description=f"{ctx.author.mention}, the suggestions emoji has been successfully unbound",
            color=discord.Color.blue(),
        )
        async with self.bot.db.acquire() as con:
            entry = await con.fetchval(f"SELECT emoji FROM suggestions WHERE guild_id={ctx.guild.id}")
            if entry:
                await con.execute(
                    f"UPDATE suggestions SET emoji=NULL WHERE suggestions.guild_id={ctx.guild.id}"
                )
            else:
                embed.title = "Suggestions Emoji Not Bound"
                embed.description = f"{ctx.author.mention}, no emoji was previously bound. Cannot unbind nonexistent emoji"
                embed.color = discord.Color.dark_red()
        return await ctx.channel.send(embed=embed)

    @suggestions.group(invoke_without_command=True,
                       name="threshold",
                       aliases=["count"])
    async def suggestions_threshold(self, ctx: Context):
        return await self.show_help(ctx)

    @suggestions_threshold.command(name="set",)
    @commands.has_permissions(manage_guild=True,)
    async def suggestions_threshold_set(self, ctx: Context, value: str):
        embed = discord.Embed(
            title="Suggestions Threshold Set",
            description=f"{ctx.author.mention}, ",
            color=discord.Color.blue(),
        )
        try:
            value = int(value)
            if not 1 <= value:
                raise ValueError()
            async with self.bot.db.acquire() as con:
                await con.execute(
                    f"INSERT INTO suggestions(guild_id, threshold) VALUES ({ctx.guild.id}, {value}) ON CONFLICT "
                    f"(guild_id) DO UPDATE SET threshold=EXCLUDED.threshold WHERE suggestions.guild_id={ctx.guild.id}"
                )
            embed.description += f"the threshold has been set to {value} reaction{'s' if value != 1 else ''} for a message to be sent to the suggestions channel"
        except (ValueError, TypeError):
            embed.title = "Invalid Value"
            embed.description += f"{value} must be a non-zero positive integer"
        finally:
            return await ctx.channel.send(embed=embed)

    @suggestions_threshold.command(name="reset",)
    @commands.has_permissions(manage_guild=True,)
    async def suggestions_threshold_reset(self, ctx: Context):
        embed = discord.Embed(
            title="Suggestions Threshold Reset",
            description=f"{ctx.author.mention}, the suggestions threshold will now be calculated based on the formula",
            color=discord.Color.blue(),
        )
        async with self.bot.db.acquire() as con:
            entry = await con.fetchval(f"SELECT threshold FROM suggestions WHERE guild_id={ctx.guild.id}")
            if entry:
                await con.execute(
                    f"UPDATE suggestions SET threshold=NULL WHERE suggestions.guild_id={ctx.guild.id}"
                )
            else:
                embed.title = "Suggestions Threshold Already Reset"
                embed.description = f"{ctx.author.mention}, the threshold already follow the formula. There is no need to reset it"
                embed.color = discord.Color.dark_red()
        return await ctx.channel.send(embed=embed)

    @suggestions.command(name="enable",
                         aliases=["start"])
    @commands.has_permissions(manage_guild=True,)
    async def suggestions_enable(self, ctx: Context):
        embed = discord.Embed(
            title="Suggestions Enabled",
            description=f"{ctx.author.mention}, suggestions have been enabled on this server",
            color=discord.Color.blue(),
        )
        async with self.bot.db.acquire() as con:
            entry = await con.fetchval(f"SELECT guild_id FROM suggestions WHERE enabled=TRUE and guild_id={ctx.guild.id}")
            if entry:
                embed.description = f"{ctx.author.mention}, suggestions are already enabled"
            else:
                await con.execute(
                    f"INSERT INTO suggestions(guild_id, enabled) VALUES ({ctx.guild.id}, TRUE) ON CONFLICT "
                    "(guild_id) DO UPDATE SET enabled=TRUE WHERE suggestions.guild_id=EXCLUDED.guild_id"
                )
        return await ctx.channel.send(embed=embed)

    @suggestions.command(name="disable",
                         aliases=["stop"])
    @commands.has_permissions(manage_guild=True,)
    async def suggestions_disable(self, ctx: Context):
        embed = discord.Embed(
            title="Suggestions Disabled",
            description=f"{ctx.author.mention}, suggestions have been disabled on this server",
            color=discord.Color.blue(),
        )
        async with self.bot.db.acquire() as con:
            entry = await con.fetchval(
                f"SELECT guild_id FROM suggestions WHERE enabled=FALSE and guild_id={ctx.guild.id}"
            )
            if entry:
                embed.description = f"{ctx.author.mention}, suggestions are already disabled"
            else:
                await con.execute(
                    f"INSERT INTO suggestions(guild_id, enabled) VALUES ({ctx.guild.id}, FALSE) ON CONFLICT "
                    "(guild_id) DO UPDATE SET enabled=FALSE WHERE suggestions.guild_id=EXCLUDED.guild_id"
                )
        return await ctx.channel.send(embed=embed)


def setup(bot: AutoShardedBot) -> None:
    bot.add_cog(Suggestions(bot))
