import os
import random
from typing import List, Tuple

import aiohttp
import discord
from discord.ext import commands, tasks
from discord.ext.commands import AutoShardedBot
from pantheon import pantheon
from pantheon.utils.exceptions import Forbidden as PantheonForbidden, NotFound as PantheonNotFound
from utils.customerrors import PantheonAPIError, PanthoenSummonerNotFound

from utils.lol import LeagueChampion, LeagueChampionItemConfig, LeagueChampionRuneConfig, _SR_SUMS, _HA_SUMS, _ROLES

_BASE_CHAMPION_INFO_URL = "https://ddragon.leagueoflegends.com/cdn/{}/data/en_US/champion/{}.json"

rito_api_key = os.environ.get("RIOTAPI")
if rito_api_key is None:
    def setup(*args, **kwargs) -> None:
        pass
else:
    try:
        panth = pantheon.Pantheon("NA1", rito_api_key, errorHandling=False)
    except:
        def setup(*args, **kwargs) -> None:
            pass
    else:
        def setup(bot: AutoShardedBot) -> None:
            bot.add_cog(LeagueofLegends(bot))


class LeagueofLegends(commands.Cog):
    def __init__(self, bot: AutoShardedBot) -> None:
        self.bot = bot
        self._ddragon_version = 0
        self._ddragon_champions = {}
        self.bot.loop.create_task(self._init_champions())
        self._first_time = True

    @tasks.loop(hours=24)
    async def _init_champions(self) -> None:
        async with aiohttp.ClientSession() as cs:
            # get current patch
            async with cs.get("https://ddragon.leagueoflegends.com/api/versions.json") as resp:
                version_res = await resp.json()
                self._first_time = version_res[0] != self._ddragon_version
                self._ddragon_version = version_res[0]

            # get all champ data
            async with cs.get(f"https://ddragon.leagueoflegends.com/cdn/{self._ddragon_version}/data/en_US/champion.json") as resp:
                champ_res = await resp.json()
            for champion in champ_res.get("data", {}).values():
                async with cs.get(f"https://ddragon.leagueoflegends.com/cdn/{self._ddragon_version}/data/en_US/champion/{champion.get('id')}.json") as resp:
                    res = await resp.json()
                    champion["skins"] = res["data"][champion.get("id")]["skins"]
                    self._ddragon_champions[f'{champion.get("key")}'] = champion
        return

    async def get_summoner(self, ctx: commands.Context, summoner_name: str) -> dict:
        try:
            summoner = await panth.getSummonerByName(summoner_name)
        except PantheonForbidden:
            raise PantheonAPIError
        except PantheonNotFound:
            raise PanthoenSummonerNotFound(ctx, summoner_name)
        if summoner.get("id", None) is None:
            raise PanthoenSummonerNotFound(ctx, summoner_name)
        return summoner

    async def _get_random_champion(self, summoner: dict) -> LeagueChampion:
        masteries = await panth.getChampionMasteries(summoner.get("id"))
        total_points = sum([champion.get("championPoints") for champion in masteries])
        mastery_weights = [1 - champion.get("championPoints") / total_points for champion in masteries]
        champ_selected = (random.choices(masteries, weights=mastery_weights, k=1))[0]
        mastery_points = champ_selected["championPoints"]
        mastery_level = champ_selected["championLevel"]
        champ_data = self._ddragon_champions[f'{champ_selected["championId"]}']
        return LeagueChampion(self._ddragon_version, champ_data, points=mastery_points, level=mastery_level)

    async def _get_random_skin(self, champion_id: str, champion_name: str) -> "Tuple(str, int)":
        async with aiohttp.ClientSession() as cs:
            async with cs.get(_BASE_CHAMPION_INFO_URL.format(self._ddragon_version, champion_id)) as resp:
                res = await resp.json()
        skins = res["data"][champion_id]["skins"]
        selected_skin = random.choice(skins)
        skin_name = selected_skin["name"] if selected_skin["name"] != "default" else f"Classic {champion_name}"
        skin_num = selected_skin["num"]
        return skin_name, skin_num

    async def _get_random_runes(self) -> LeagueChampionRuneConfig:
        return LeagueChampionRuneConfig.from_random()

    async def _get_random_sums(self, mode: str, smite: bool = True, force_smite: bool = False) -> List[str]:
        if force_smite:
            sums = _SR_SUMS.copy()
            sums.remove("Smite")
            return ["Smite", random.choice(sums)]
        if smite or mode == "aram":
            return random.sample(_SR_SUMS if mode == "sr" else _HA_SUMS, 2)
        sums = _SR_SUMS.copy()
        sums.remove("Smite")
        return random.sample(sums, 2)

    async def _get_random_items(self, champion: LeagueChampion, mode: str, limit: List[str] = []) -> LeagueChampionItemConfig:
        return LeagueChampionItemConfig.from_random(champion, mode=mode, limit=limit)

    async def _generate_embed(self, mode: str, *, champion: LeagueChampion, runes: LeagueChampionRuneConfig, sums: List[str], items: LeagueChampionItemConfig, role: str = None, summoner: dict = None) -> discord.Embed:
        _apos = "'"
        embed = discord.Embed(
            title=f"{champion.name}, {champion.title}" if role is None else f"{role} | {champion.name}, {champion.title}",
            color=discord.Color.blue(),
        ).set_thumbnail(
            url=champion.square_url,
        ).set_image(
            url=champion.get_skin_splash(randomise=True),
        ).set_footer(
            text=f"{champion._last_rolled_skin} | Mastery {champion.mastery_level} | {champion.mastery_points:,} pts | "
            f"Randomised {f'Summoner{_apos}s Rift' if mode.lower() == 'sr' else 'Howling Abyss'} Configuration"
        )
        _nl = '\n'
        if runes:
            embed.add_field(
                name=f"Primary Tree: {runes.primary_tree}",
                value=f"""- {runes.keystone}
- {runes.p1}
- {runes.p2}
- {runes.p3}""",
                inline=False,
            ).add_field(
                name=f"Secondary Tree: {runes.secondary_tree}",
                value=f"""- {runes.s1}
- {runes.s2}""",
                inline=False,
            ).add_field(
                name="Minor Runes",
                value=f"{_nl.join(['- {}'.format(minor_rune) for minor_rune in runes.minor_runes])}",
                inline=True,
            )

        if sums:
            embed.add_field(
                name=f"Summoner Spells",
                value=f"{_nl.join(['- {}'.format(spell) for spell in sums])}"
            )

        if items:
            item_des = f"- {items.mythic}"
            for item in items.legendaries:
                item_des += f"\n- {item}"
            item_des += f"\n- {items.boots}" if items.boots is not None else ""
            embed.add_field(
                name="Items",
                value=item_des,
                inline=False,
            )

        if summoner:
            embed.set_author(
                name=summoner.get("name"),
                url=f"https://na.op.gg/summoners/na/{(summoner.get('name')).replace(' ', '%20')}",
                icon_url=f"http://ddragon.leagueoflegends.com/cdn/{self._ddragon_version}/img/profileicon/{summoner.get('profileIconId')}.png"
            )
        return embed

    @commands.cooldown(1, 10, commands.BucketType.user)
    @commands.command(aliases=["lolrc"],
                      desc="Selects a random champion from a summoner's champion pool",
                      usage="randomchamp [summoner_name]")
    async def randomchamp(self, ctx: commands.Context, *, summoner_name: str):
        summoner = await self.get_summoner(ctx, summoner_name)
        champion = await self._get_random_champion(summoner)
        return await ctx.channel.send(
            embed=await self._generate_embed("sr", champion=champion, summoner=summoner)
        )

    @commands.cooldown(1, 10, commands.BucketType.member)
    @commands.command(aliases=["lolrcc"],
                      desc="Selects a random champion from a summoner's champion pool with a random configuration",
                      usage="randomchampconfig [mode] [summoner_name]",
                      note="`[mode]` must be either \"SR\" or \"ARAM\"")
    async def randomchampconfig(self, ctx: commands.Context, mode: str, *, summoner_name: str):
        if mode.lower() not in ["sr", "aram"]:
            return await ctx.channel.send(
                embed=discord.Embed(
                    title="Invalid Mode Specified",
                    description=f"{ctx.author.mention}, please specify either **SR** or **ARAM** as the mode",
                    color=discord.Color.dark_red(),
                )
            )
        summoner = await self.get_summoner(ctx, summoner_name)
        champion = await self._get_random_champion(summoner)
        runes = await self._get_random_runes()
        sums = await self._get_random_sums(mode.lower())
        items = await self._get_random_items(champion, mode)
        embed = await self._generate_embed(mode=mode, champion=champion, runes=runes, sums=sums, items=items, summoner=summoner)
        return await ctx.channel.send(embed=embed)

    @commands.cooldown(1, 10, commands.BucketType.member)
    @commands.command(aliases=["lolrccl"],
                      desc="Selects a random champion from a summoner's champion pool with a random configuration specifically for laning",
                      usage="randomchampconfiglane [summoner_name]",)
    async def randomchampconfiglane(self, ctx: commands.Context, *, summoner_name: str):
        summoner = await self.get_summoner(ctx, summoner_name)
        champion = await self._get_random_champion(summoner)
        runes = await self._get_random_runes()
        sums = await self._get_random_sums("SR", smite=False)
        items = await self._get_random_items(champion, "SR", limit=["support"])
        embed = await self._generate_embed(mode="SR", champion=champion, runes=runes, sums=sums, items=items, role="Lane", summoner=summoner)
        return await ctx.channel.send(embed=embed)

    @commands.cooldown(1, 10, commands.BucketType.member)
    @commands.command(aliases=["lolrccj"],
                      desc="Selects a random champion from a summoner's champion pool with a random configuration specifically for jungling",
                      usage="randomchampconfigjungle [summoner_name]",)
    async def randomchampconfigjungle(self, ctx: commands.Context, *, summoner_name: str):
        summoner = await self.get_summoner(ctx, summoner_name)
        champion = await self._get_random_champion(summoner)
        runes = await self._get_random_runes()
        sums = await self._get_random_sums("SR", force_smite=True)
        items = await self._get_random_items(champion, "SR", limit=["support"])
        embed = await self._generate_embed(mode="SR", champion=champion, runes=runes, sums=sums, items=items, role="Jungle", summoner=summoner)
        return await ctx.channel.send(embed=embed)

    @commands.cooldown(1, 10, commands.BucketType.member)
    @commands.command(aliases=["lolrccs"],
                      desc="Selects a random champion from a summoner's champion pool with a random configuration specifically for support",
                      usage="randomchampconfigsupport [summoner_name]",)
    async def randomchampconfigsupport(self, ctx: commands.Context, *, summoner_name: str):
        summoner = await self.get_summoner(ctx, summoner_name)
        champion = await self._get_random_champion(summoner)
        runes = await self._get_random_runes()
        sums = await self._get_random_sums("SR", smite=False)
        items = await self._get_random_items(champion, "SR", limit=["force_support"])
        embed = await self._generate_embed(mode="SR", champion=champion, runes=runes, sums=sums, items=items, role="Support", summoner=summoner)
        return await ctx.channel.send(embed=embed)

    @commands.cooldown(1, 10, commands.BucketType.member)
    @commands.command(aliases=["lolrtc"],
                      desc="Randomly generates a teamcomp for League of Legends Summoner's Rift 5v5",
                      usage="randomteamconfig [summoner_name]*5",
                      note="Summoner order should be top, jungle, middle, bottom, and support, with commas separating each summoner name")
    async def randomteamconfig(self, ctx: commands.Context, *, summoner_names: str):
        summoner_names = summoner_names.split(",")
        summoners = []
        for summoner_name in summoner_names:
            summoner = await self.get_summoner(ctx, summoner_name)
            summoners.append(summoner)
        champions = []
        embeds = []
        for index, summoner in enumerate(summoners):
            for _ in range(100):
                champion = await self._get_random_champion(summoner)
                if champion.id not in champions:
                    champions.append(champion.id)
                    break
            else:
                return await ctx.channel.send(
                    embed=discord.Embed(
                        title="Error: Could Not Generate Team",
                        description=f"{ctx.author.mention}, a random team could not be generated. This may occur when all of the possible champions have been selected, leaving a summoner with no remaining options",
                        color=discord.Color.dark_red(),
                    )
                )
            runes = await self._get_random_runes()
            if index == 1:
                sums = await self._get_random_sums("SR", force_smite=True)
                items = await self._get_random_items(champion, "SR", limit=["support"])
            elif index == 4:
                sums = await self._get_random_sums("SR", smite=False)
                items = await self._get_random_items(champion, "SR", limit=["force_support"])
            else:
                sums = await self._get_random_sums("SR", smite=False)
                items = await self._get_random_items(champion, "SR", limit=["support"])
            embeds.append(await self._generate_embed(mode="SR", champion=champion, runes=runes, sums=sums, items=items, role=_ROLES[index], summoner=summoner))

        for embed in embeds:
            await ctx.channel.send(embed=embed)
        return
