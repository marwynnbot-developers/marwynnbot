import datetime
import os

import discord
from aiohttp import web
from discord.ext import commands
from discord.ext.commands import AutoShardedBot
from server import HTTPServer, add_route


class API(commands.Cog):
    def __init__(self, bot: AutoShardedBot) -> None:
        self.bot = bot
        self.bot.loop.create_task(self._init_http_server())

    async def _init_http_server(self) -> None:
        await self.bot.wait_until_ready()
        if not hasattr(self.bot, "http_server"):
            await self._http_server_startup()
        else:
            await self.bot.http_server.stop()
            await self._http_server_startup()
        return

    async def _http_server_startup(self) -> None:
        self.bot.http_server = HTTPServer(
            bot=self.bot,
            host=os.getenv("API_HOST"),
            port=os.getenv("API_PORT"),
        )
        return await self.bot.http_server.start()

    @add_route(path="/", method="GET", cog="API")
    async def echo(self, request: web.Request) -> web.Response:
        return web.json_response(data={"status": 200}, status=200)

    @add_route(path="/uptime", method="GET", cog="API")
    async def uptime(self, request: web.Request) -> web.Response:
        return web.json_response(data={"uptime": int(datetime.datetime.now().timestamp()) - self.bot.uptime}, status=200)

    @add_route(path="/stats", method="GET", cog="API")
    async def stats(self, request: web.Request) -> web.Response:
        payload = {
            "commands": len(self.bot.commands),
            "guilds": len(self.bot.guilds),
            "users": len(self.bot.users),
        }
        return web.json_response(data=payload, status=200)

    @add_route(path="/commands", method="GET", cog="API")
    async def commands(self, request: web.Request) -> web.Response:
        return web.json_response(data={"commands": len(self.bot.commands), }, status=200)

    @add_route(path="/guilds", method="GET", cog="API")
    async def guilds(self, request: web.Request) -> web.Response:
        return web.json_response(data={"guilds": len(self.bot.guilds), }, status=200)

    @add_route(path="/users", method="GET", cog="API")
    async def users(self, request: web.Request) -> web.Response:
        return web.json_response(data={"users": len(self.bot.users), }, status=200)

    @add_route(path="/guild", method="GET")
    async def guild(self, request: web.Request) -> web.Response:
        guild_id = request.rel_url.query["id"]
        guild: discord.Guild = self.bot.get_guild(int(guild_id))


def setup(bot: AutoShardedBot) -> None:
    bot.add_cog(API(bot))
