import discord
from discord.ext import commands
from discord.ext.commands import AutoShardedBot, Context


class AntiRaid(commands.Cog):
    def __init__(self, bot: AutoShardedBot) -> None:
        self.bot = bot
        self.bot.loop.create_task(self._init_tables())
        self.bot._raid_mode_data = {}

    @commands.Cog.listener()
    async def on_member_join(self, member: discord.Member):
        guild: discord.Guild = member.guild
        if self.bot._raid_mode_data.get(guild.id, False):
            try:
                await member.kick(reason="Automatically kicked by MarwynnBot's AntiRaid system")
                if not member.bot:
                    await member.send(
                        embed=discord.Embed(
                            title="Sorry!",
                            description=f"{member.mention}, we're sorry you were kicked! A raid is in place and the server is not accepting any new members at this time",
                            color=discord.Color.blue(),
                        )
                    )
            except discord.Forbidden:
                pass
        return

    async def _init_tables(self) -> None:
        await self.bot.wait_until_ready()
        async with self.bot.db.acquire() as con:
            await con.execute(
                "CREATE TABLE IF NOT EXISTS raidmode(guild_id BIGINT PRIMARY KEY, enabled BOOLEAN DEFAULT FALSE)"
            )
            entries = await con.fetch(
                "SELECT guild_id FROM raidmode WHERE enabled=TRUE"
            )
            for entry in entries:
                self.bot._raid_mode_data[int(entry["guild_id"])] = True

    async def _set_raid_mode(self, guild_id: int, *, enabled: bool) -> None:
        async with self.bot.db.acquire() as con:
            await con.execute(
                f"INSERT INTO raidmode(guild_id, enabled) VALUES ({guild_id}, {enabled}) ON CONFLICT "
                f"(guild_id) DO UPDATE SET enabled={enabled} WHERE raidmode.guild_id=EXCLUDED.guild_id"
            )
        self.bot._raid_mode_data[guild_id] = enabled

    @commands.command(aliases=["noraid"],
                      desc="Enables or disables raid mode",
                      usage="raidmode (on | off)",
                      uperms=["Manage Server"],
                      note="When raid mode is enabled, new users and bots that attempt to join will be automatically kicked. "
                      "If `(on | off)` is unspecified, it will display the server's current raid mode")
    @commands.has_permissions(manage_guild=True)
    @commands.bot_has_permissions(kick_members=True)
    async def raidmode(self, ctx: Context, on_or_off: str = None):
        if on_or_off is None:
            async with self.bot.db.acquire() as con:
                raid_enabled = (await con.fetchval(f"SELECT enabled FROM raidmode WHERE guild_id={ctx.guild.id}")) or False
                raid_mode = "on" if raid_enabled else "off"
            return await ctx.channel.send(
                embed=discord.Embed(
                    title=f"Raid Mode - {raid_mode.title()}",
                    description=f"{ctx.author.mention}, raid mode is currently **{raid_mode}**",
                    color=discord.Color.blue(),
                )
            )
        if not on_or_off.lower() in ["on", "off"]:
            return await ctx.channel.send(
                embed=discord.Embed(
                    title="Invalid Raid Mode",
                    description=f"{ctx.author.mention}, the raid mdoe `{on_or_off}` is not a valid raid mode",
                    color=discord.Color.dark_red(),
                )
            )
        enabled = on_or_off.lower() == "on"
        raid_mode = "on" if enabled else "off"
        message = "No new users and bots will be able to join until raid mode is turned off" if enabled else "New users and bots can now join the server"
        await self._set_raid_mode(ctx.guild.id, enabled=enabled)
        return await ctx.channel.send(
            embed=discord.Embed(
                title=f"Raid Mode - {raid_mode.title()}",
                description=f"{ctx.author.mention}, raid mode has been turned **{raid_mode}**. {message}",
                color=discord.Color.blue(),
            )
        )


def setup(bot: AutoShardedBot) -> None:
    bot.add_cog(AntiRaid(bot))
