import asyncio
import datetime
import random
from typing import Any, List, NamedTuple, Union

import discord
from discord.ext import commands
from discord.ext.commands import AutoShardedBot, Context
from utils import GlobalCMDS, SubcommandHelp
from utils.paginator import FieldPaginator

CONF_REACTIONS = ["✅", "🛑"]


class AutoreplyEntry(NamedTuple):
    author_id: int = None
    guild_id: int = None
    target_id: int = None
    responses: List[str] = []
    active: bool = True
    expiration: int = None


class Autoreply(commands.Cog):
    def __init__(self, bot: AutoShardedBot):
        self.bot = bot
        self.gcmds = GlobalCMDS(bot)
        self.autoreply_cache = {}
        self._autoreply_fut = asyncio.Future()
        for func in [self.init_autoreply, self.init_cache]:
            self.bot.loop.create_task(func())
        self.tasks = []

    def cog_unload(self):
        for task in self.tasks:
            task.cancel()

    async def cog_command_error(self, ctx: Context, error):
        if isinstance(error, commands.errors.UserNotFound):
            return await ctx.channel.send(embed=discord.Embed(
                title="User Not Found",
                description=f"{ctx.author.mention}, the user `{error.argument}` could not be found",
                color=discord.Color.dark_red()
            ))

    async def init_autoreply(self) -> None:
        await self.bot.wait_until_ready()
        async with self.bot.db.acquire() as con:
            await con.execute(
                "CREATE TABLE IF NOT EXISTS autoreply(author_id bigint, guild_id bigint, "
                "target_id bigint, responses TEXT[], active BOOLEAN DEFAULT TRUE, expiration NUMERIC)"
            )
        self._autoreply_fut.set_result(True)

    async def init_cache(self) -> None:
        await self.bot.wait_until_ready()
        await self._autoreply_fut
        async with self.bot.db.acquire() as con:
            entries = await con.fetch("SELECT * FROM autoreply")
        if entries:
            for entry in entries:
                task = self.bot.loop.create_task(self._place_in_cache(entry=entry))
                task.add_done_callback(self._handle_task_result)

    @commands.Cog.listener()
    async def on_message(self, message: discord.Message) -> None:
        if message.guild and message.content and not message.author.bot:
            pfx = await self.gcmds.prefix(None, guild_id=message.guild.id)
            if message.content.startswith(pfx) or message.content.lower().startswith("mb "):
                return
            target_id = message.author.id
            try:
                entry: AutoreplyEntry = self.autoreply_cache[message.guild.id].get(target_id)
                if entry.active:
                    return await message.channel.send(content=random.choice(entry.responses))
            except (KeyError, AttributeError, discord.Forbidden, discord.HTTPException):
                pass

    async def _place_in_cache(self, entry: Any = None,
                              author_id: int = None, guild_id: int = None,
                              target_id: int = None, responses: List[str] = [],
                              active: bool = True, expiration: int = None) -> None:
        current_timestamp = int(datetime.datetime.now().timestamp())
        if entry:
            author_id = author_id if author_id is not None else entry["author_id"]
            guild_id = guild_id if guild_id is not None else entry["guild_id"]
            target_id = target_id if target_id is not None else entry["target_id"]
            responses = responses if responses else [resp.replace(
                "$comma$", ",").replace("$rp$", "") for resp in entry["responses"]]
            active = entry["active"]
            expiration = expiration if expiration is not None else entry["expiration"]
        if not guild_id in self.autoreply_cache:
            self.autoreply_cache[guild_id] = {}
        entry = AutoreplyEntry(
            author_id=author_id,
            guild_id=guild_id,
            target_id=target_id,
            responses=responses,
            active=active,
            expiration=expiration,
        )
        self.autoreply_cache[guild_id][target_id] = entry
        self.tasks.append(
            self.bot.loop.create_task(self._expire(entry, expiration - current_timestamp))
        )

    @ staticmethod
    def _handle_task_result(task: asyncio.Task) -> None:
        try:
            task.result()
        except Exception:
            pass

    async def _expire(self, entry: AutoreplyEntry, time_to_wait: int = 60 ** 2 * 4, ret: bool = False) -> discord.Embed:
        await asyncio.sleep(time_to_wait)
        async with self.bot.db.acquire() as con:
            await con.execute(f"DELETE FROM autoreply WHERE author_id={entry.author_id} AND guild_id={entry.guild_id} AND target_id={entry.target_id}")
        try:
            del self.autoreply_cache[entry.guild_id][entry.target_id]
        except KeyError:
            if ret:
                return discord.Embed(
                    title="No Autoreply Found",
                    description=f"I could not find an autoreply with the target <@{entry.target_id}>",
                    color=discord.Color.dark_red()
                )
        if ret:
            return discord.Embed(
                title="Autoreply Deleted",
                description=f"The autoreply with the target <@{entry.target_id}> has been deleted",
                color=discord.Color.blue()
            )

    async def get_autoreplies(self, guild_id: int, target: discord.User = None) -> Union[List[AutoreplyEntry], AutoreplyEntry]:
        entries = self.autoreply_cache.get(guild_id)
        if not entries:
            return None
        if target:
            return entries.get(target.id)
        return [
            AutoreplyEntry(
                author_id=entry["author_id"],
                guild_id=entry["guild_id"],
                target_id=entry["target_id"],
                responses=entry["responses"],
                active=entry["active"],
                expiration=entry["expiration"],
            ) for entry in entries
        ]

    async def check_autoreply_exists(self, ctx: Context, target: discord.User, opposite: bool = False) -> None:
        exists = await self.get_autoreplies(ctx.guild.id, target=target)
        if not exists:
            if not opposite:
                await ctx.channel.send(embed=discord.Embed(
                    title="No Autoreply Exists",
                    description=f"{ctx.author.mention}, there is no autoreply set for target {target.mention}",
                    color=discord.Color.dark_red(),
                ))
            raise ValueError
        if not opposite:
            perms: discord.Permissions = ctx.channel.permissions_for(ctx.author)
            if not perms.manage_guild and not (ctx.author.id == exists.author_id or ctx.author.id == target.id):
                await ctx.channel.send(embed=discord.Embed(
                    title="Invalid Permissions",
                    description=f"{ctx.author.mention}, you are not the owner or the target of this autoreply",
                    color=discord.Color.dark_red(),
                ))
                raise ValueError
        return exists

    async def confirm_consent(self, ctx: Context, target: discord.User, op: str = None):
        embed = discord.Embed(
            title="Confirm Consent",
            description=(f"{target.mention}, {ctx.author.mention} would like to {op} an autoreply with you as its target. "
                         f"React with {CONF_REACTIONS[0]} to confirm your consent within 60 seconds, otherwise it will be canceled"),
            color=discord.Color.blue()
        )
        panel_msg = await ctx.channel.send(embed=embed)
        for reaction in CONF_REACTIONS:
            await panel_msg.add_reaction(reaction)

        try:
            reaction, _ = await self.bot.wait_for("reaction_add", check=lambda r, u: r.message == panel_msg and r.emoji in CONF_REACTIONS and u.id == target.id, timeout=60)
        except asyncio.TimeoutError:
            await self.gcmds.timeout(ctx, f"{op.title()} Autoreply", 60)
            raise ValueError
        if reaction.emoji == CONF_REACTIONS[1]:
            await self.gcmds.canceled(ctx, f"{op.title()} Autoreply")
            raise ValueError

    @staticmethod
    def _get_pg_safe_responses(responses: List[str]) -> str:
        return "$resp${" + ", ".join([f"$rp${response.replace(',', '$comma$')}$rp$" for response in responses]) + "}$resp$"

    async def create_autoreply(self, *,
                               author_id: int, guild_id: int,
                               target_id: int, responses: List[str],
                               expiration: int) -> discord.Embed:
        pg_safe_responses = self._get_pg_safe_responses(responses)
        async with self.bot.db.acquire() as con:
            values = ", ".join([
                str(author_id),
                str(guild_id),
                str(target_id),
                pg_safe_responses,
                str(expiration),
            ])
            await con.execute(f"INSERT INTO autoreply(author_id, guild_id, target_id, responses, expiration) VALUES ({values})")
        await self._place_in_cache(
            author_id=author_id,
            guild_id=guild_id,
            target_id=target_id,
            responses=responses,
            expiration=expiration
        )
        return discord.Embed(
            title="Successfully Created Autoreply",
            description=f"<@{author_id}>, the autoreply was created for target <@{target_id}>",
            color=discord.Color.blue()
        )

    async def edit_autoreply(self, *,
                             author_id: int, guild_id: int,
                             target_id: int, responses: List[str],
                             active: bool, expiration: int) -> discord.Embed:
        pg_safe_responses = self._get_pg_safe_responses(responses)
        async with self.bot.db.acquire() as con:
            await con.execute(
                f"UPDATE autoreply SET responses={pg_safe_responses}, expiration={expiration} WHERE author_id={author_id} AND target_id={target_id} AND guild_id={guild_id}"
            )
        try:
            del self.autoreply_cache[guild_id][target_id]
        except KeyError:
            pass
        await self._place_in_cache(
            author_id=author_id,
            guild_id=guild_id,
            target_id=target_id,
            responses=responses,
            active=active,
            expiration=expiration,
        )
        return discord.Embed(
            title="Successfully Edited Autoreply",
            description=f"<@{author_id}>, the autoreply was edited for target <@{target_id}>",
            color=discord.Color.blue()
        )

    async def delete_autoreply(self, entry: AutoreplyEntry, time_to_wait: int = 0, ret: bool = True) -> discord.Embed:
        return await self._expire(entry, time_to_wait=time_to_wait, ret=ret)

    async def manage_active_status(self, ctx: Context, target: discord.User, *, active: bool) -> discord.Embed:
        async with self.bot.db.acquire() as con:
            await con.execute(f"UPDATE autoreply SET active={'TRUE' if active else 'FALSE'} WHERE guild_id={ctx.guild.id} AND target_id={target.id}")
        existing = await self.get_autoreplies(ctx.guild.id, target=target)
        try:
            del self.autoreply_cache[ctx.guild.id][target.id]
        except KeyError:
            pass
        await self._place_in_cache(
            author_id=existing.author_id,
            guild_id=existing.guild_id,
            target_id=existing.target_id,
            responses=existing.responses,
            active=active,
            expiration=existing.expiration,
        )
        return discord.Embed(
            title=f"Autoreply {'Activated' if active else 'Deactivated'}",
            description=f"{ctx.author.mention}, the autoreply for the target {target.mention} has been {'activated' if active else 'deactivated'}",
            color=discord.Color.blue(),
        )

    @commands.group(invoke_without_command=True,
                    aliases=["arply", "arpl"],
                    desc="Displays the help command for autoreply",
                    usage="autoreply (subcommand)",)
    async def autoreply(self, ctx):
        pfx = f"{await self.gcmds.prefix(ctx)}autoreply"
        return await SubcommandHelp(
            pfx=pfx,
            title="Autoreply Subcommand Help",
            description=(
                f"{ctx.author.mention}, MarwynnBot's autoreply feature allow you to customise a response or list of responses "
                "to be said immediately after a message by a certain member has been sent. Server members with the `Manage Server` "
                f"permission may freely do any operation. The base command is `{pfx}`. Here are all valid subcommands"
            ),
            per_page=2,
            show_entry_count=True,
        ).from_config("autoreply").show_help(ctx)

    @autoreply.command(name="list",
                       aliases=["ls", "show"],)
    async def autoreply_list(self, ctx, target: discord.User = None):
        try:
            if not target:
                target = ctx.author
            entries = await self.check_autoreply_exists(ctx, target)
        except ValueError:
            return

        responses = [
            f"```{response.replace('$resp$', '')}```" for response in entries.responses
        ]
        description = "\n".join([
            f"**Author:** <@{entries.author_id}>",
            f"**Active:** `{'Yes' if entries.active else 'No'}`",
            f"**Expires In:** `{datetime.timedelta(seconds=int(float(entries.expiration) - datetime.datetime.now().timestamp()))}`",
            "**Possible Replies:**\n\n",
        ])
        return await FieldPaginator(
            ctx,
            entries=[(f"Reply {index}", response, True) for index, response in enumerate(responses, 1)],
            per_page=6,
            show_entry_count=True,
            embed=discord.Embed(title=f"Autoreply Target: {target.display_name}", description=description, color=discord.Color.blue(
            )).set_thumbnail(url=target.avatar_url),
            description=description
        ).paginate()

    @autoreply.command(name="create",
                       aliaes=["c", "make"],)
    async def autoreply_create(self, ctx, target: discord.User):
        try:
            await self.check_autoreply_exists(ctx, target, opposite=True)
            return await ctx.channel.send(embed=discord.Embed(
                title="Autoreply Already Exists",
                description=f"{ctx.author.mention}, an autoreply for the target {target.mention} already exists",
                color=discord.Color.dark_red()
            ))
        except ValueError:
            pass

        try:
            await self.confirm_consent(ctx, target, op="Create")
        except ValueError:
            return

        embed = discord.Embed(
            title="Create Autoreply",
            description=str(
                f"{ctx.author.mention}, within 120 seconds, please enter a reply to be sent after {target.mention} sends a message, "
                "or enter \"finish\" to complete setup"),
            color=discord.Color.blue()
        ).set_footer(
            text="The reply must be no longer than 200 characters. Enter \"cancel\" to cancel at any time"
        )

        responses = []

        while True:
            await ctx.channel.send(embed=embed)
            try:
                message = await self.bot.wait_for("message", check=lambda m: m.author == ctx.author and m.channel == ctx.channel, timeout=120)
            except asyncio.TimeoutError:
                return await self.gcmds.timeout(ctx, "Create Autoreply", 120)
            if message.content == "finish":
                break
            elif message.content == "cancel":
                return await self.gcmds.canceled(ctx, "Create Autoreply")
            elif message.content and len(message.content) <= 200:
                responses.append(message.content)

        embed: discord.Embed = embed.copy()
        embed.description = str(f"{ctx.author.mention}, enter the number of hours this autoreply should expire in, or "
                                "\"default\" to use the default time")
        embed.set_footer(
            text="Maximum expiration time is 4 hours. Enter cancel to cancel at anytime"
        )
        await ctx.channel.send(embed=embed)
        current_timestamp = int(datetime.datetime.now().timestamp())
        try:
            message = await self.bot.wait_for("message", check=lambda m: m.author == ctx.author and m.channel == ctx.channel, timeout=120)
            if message.content.lower() == "cancel":
                return await self.gcmds.canceled(ctx, "Create Autoreply")
            if float(message.content) > 4:
                raise AssertionError
            expiration = current_timestamp + int(float(message.content) * 60 ** 2)
        except asyncio.TimeoutError:
            return await self.gcmds.timeout(ctx, "Create Autoreply", 120)
        except AssertionError:
            return await ctx.channel.send(embed=discord.Embed(
                title="Invalid Expiration",
                description=f"{ctx.author.mention}, the maximum expiration time is 4 hours",
                color=discord.Color.dark_red()
            ))
        except ValueError:
            expiration = current_timestamp + 4 * 60 ** 2

        return await ctx.channel.send(embed=await self.create_autoreply(
            author_id=ctx.author.id,
            guild_id=ctx.guild.id,
            target_id=target.id,
            responses=responses,
            expiration=expiration,
        ))

    @autoreply.command(name="edit",
                       aliases=["e", "modify"],)
    async def autoreply_edit(self, ctx, target: discord.User):
        try:
            exists = await self.check_autoreply_exists(ctx, target)
        except ValueError:
            return

        try:
            await self.confirm_consent(ctx, target, op="edit")
        except ValueError:
            return

        embed = discord.Embed(
            title="Edit Autoreply",
            description=str(
                f"{ctx.author.mention}, within 120 seconds, please enter a reply to be sent after {target.mention} sends a message, "
                "enter \"finish\" to complete setup, or enter \"skip\" to keep the current replies"),
            color=discord.Color.blue()
        ).set_footer(
            text="The reply must be no longer than 200 characters. Enter \"cancel\" to cancel at any time"
        )

        responses = []

        while True:
            await ctx.channel.send(embed=embed)
            try:
                message = await self.bot.wait_for("message", check=lambda m: m.author == ctx.author and m.channel == ctx.channel, timeout=120)
            except asyncio.TimeoutError:
                return await self.gcmds.timeout(ctx, "Edit Autoreply", 120)
            if message.content == "finish":
                break
            elif message.content == "cancel":
                return await self.gcmds.canceled(ctx, "Edit Autoreply")
            elif message.content == "skip":
                responses = exists.responses
                break
            elif message.content and len(message.content) <= 200:
                responses.append(message.content)

        embed: discord.Embed = embed.copy()
        embed.description = str(f"{ctx.author.mention}, enter the number of hours this autoreply should expire in, "
                                "\"default\" to use the default time, or \"skip\" to use the currently set time")
        embed.set_footer(
            text="Maximum expiration time is 4 hours. Enter cancel to cancel at anytime"
        )
        await ctx.channel.send(embed=embed)
        current_timestamp = int(datetime.datetime.now().timestamp())
        try:
            message = await self.bot.wait_for("message", check=lambda m: m.author == ctx.author and m.channel == ctx.channel, timeout=120)
            if message.content.lower() == "cancel":
                return await self.gcmds.canceled(ctx, "Edit Autoreply")
            elif message.content == "skip":
                expiration = exists.expiration
            elif float(message.content) > 4:
                raise AssertionError
            expiration = current_timestamp + int(float(message.content) * 60 ** 2)
        except asyncio.TimeoutError:
            return await self.gcmds.timeout(ctx, "Edit Autoreply", 120)
        except AssertionError:
            return await ctx.channel.send(embed=discord.Embed(
                title="Invalid Expiration",
                description=f"{ctx.author.mention}, the maximum expiration time is 4 hours",
                color=discord.Color.dark_red()
            ))
        except ValueError:
            expiration = current_timestamp + 4 * 60 ** 2

        return await ctx.channel.send(embed=await self.edit_autoreply(
            author_id=ctx.author.id,
            guild_id=ctx.guild.id,
            target_id=target.id,
            responses=responses,
            active=exists.active,
            expiration=expiration,
        ))

    @autoreply.command(name="delete",
                       aliases=["del", "d", "remove"],)
    async def autoreply_delete(self, ctx, target: discord.User):
        try:
            exists = await self.check_autoreply_exists(ctx, target)
        except ValueError:
            return

        embed = discord.Embed(
            title="Confirm Delete",
            description=(f"This action is destructive and irreversible. To proceed, react with {CONF_REACTIONS[0]} "
                         "to confirm your consent within 60 seconds, otherwise it will be canceled"),
            color=discord.Color.blue()
        )
        panel_msg = await ctx.channel.send(embed=embed)
        for reaction in CONF_REACTIONS:
            await panel_msg.add_reaction(reaction)

        try:
            reaction, _ = await self.bot.wait_for("reaction_add", check=lambda r, u: r.message == panel_msg and r.emoji in CONF_REACTIONS and u.id == ctx.author.id, timeout=60)
        except asyncio.TimeoutError:
            await self.gcmds.timeout(ctx, "Delete Autoreply", 60)
        if reaction.emoji == CONF_REACTIONS[1]:
            await self.gcmds.canceled(ctx, "Delete Autoreply")

        return await ctx.channel.send(
            embed=await self.delete_autoreply(
                exists,
                time_to_wait=0,
                ret=True
            )
        )

    @autoreply.command(name="activate",
                       aliases=["ac", "enable"],)
    async def autoreply_activate(self, ctx, target: discord.User):
        try:
            await self.check_autoreply_exists(ctx, target)
        except KeyError:
            return

        return await ctx.channel.send(
            embed=await self.manage_active_status(
                ctx, target, active=True
            )
        )

    @autoreply.command(name="deactivate",
                       aliases=["deac", "disable"],)
    async def autoreply_deactivate(self, ctx, target: discord.User):
        try:
            await self.check_autoreply_exists(ctx, target)
        except KeyError:
            return

        return await ctx.channel.send(
            embed=await self.manage_active_status(
                ctx, target, active=False
            )
        )


def setup(bot: AutoShardedBot):
    bot.add_cog(Autoreply(bot))
