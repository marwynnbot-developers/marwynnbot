from multiprocessing.sharedctypes import Value
import random
from typing import List

_SR_SUMS = [
    "Heal",
    "Ghost",
    "Barrier",
    "Exhaust",
    "Flash",
    "Teleport",
    "Smite",
    "Cleanse",
    "Ignite"
]
_HA_SUMS = _SR_SUMS + [
    "Mark",
    "Clarity",
]
_HA_SUMS.remove("Smite")
_HA_SUMS.remove("Teleport")
_MAJOR_RUNES = {
    "Precision": {
        "Keystones": [
            "Press the Attack",
            "Lethal Tempo",
            "Fleet Footwork",
            "Conqueror",
        ],
        "1": [
            "Overheal",
            "Triumph",
            "Presence of Mind",
        ],
        "2": [
            "Legend: Alacrity",
            "Legend: Tenacity",
            "Legend: Bloodline",
        ],
        "3": [
            "Coup de Grace",
            "Cut Down",
            "Last Stand",
        ],
    },
    "Domination": {
        "Keystones": [
            "Electrocute",
            "Predator",
            "Dark Harvest",
            "Hail of Blades",
        ],
        "1": [
            "Cheap Shot",
            "Taste of Blood",
            "Sudden Impact",
        ],
        "2": [
            "Zombie Ward",
            "Ghost Poro",
            "Eyeball Collection",
        ],
        "3": [
            "Ravenous Hunter",
            "Ingenious Hunter",
            "Relentless Hunter",
            "Ultimate Hunter",
        ],
    },
    "Sorcery": {
        "Keystones": [
            "Summon Aery",
            "Arcane Comet",
            "Phase Rush",
        ],
        "1": [
            "Nullifying Orb",
            "Manaflow Band",
            "Nimbus Cloak",
        ],
        "2": [
            "Transcendence",
            "Celerity",
            "Absolute Focus",
        ],
        "3": [
            "Scorch",
            "Waterwalking",
            "Gathering Storm",
        ],
    },
    "Resolve": {
        "Keystones": [
            "Grasp of the Undying",
            "Aftershock",
            "Guardian",
        ],
        "1": [
            "Demolish",
            "Font of Life",
            "Shield Bash",
        ],
        "2": [
            "Conditioning",
            "Second Wind",
            "Bone Plating",
        ],
        "3": [
            "Overgrowth",
            "Revitalise",
            "Unflinching",
        ],
    },
    "Inspiration": {
        "Keystones": [
            "Glacial Augment",
            "Unsealed Spellbook",
            "First Strike",
        ],
        "1": [
            "Hextech Flashtraption",
            "Magical Footwear",
            "Perfect Timing",
        ],
        "2": [
            "Future's Market",
            "Minion Dematerialiser",
            "Biscuit Delivery",
        ],
        "3": [
            "Cosmic Insight",
            "Approach Velocity",
            "Time Warp Tonic",
        ],
    }
}
_MINOR_RUNES = {
    "1": [
        "Adaptive",
        "Attack Speed",
        "Ability Haste",
    ],
    "2": [
        "Adaptive",
        "Armor",
        "Magic Resistance",
    ],
    "3": [
        "Health",
        "Armor",
        "Magic Resistance",
    ],
}
_BASE_SPLASH_URL = "http://ddragon.leagueoflegends.com/cdn/img/champion/splash/{}_{}.jpg"
_BASE_SQUARE_ART_URL = "https://ddragon.leagueoflegends.com/cdn/{}/img/champion/{}.png"

_LEGENDARY_ITEMS = {
    "Abyssal Mask": {
        "sr": True,
        "aram": True,
    },
    "Anathema's Chains": {
        "sr": True,
        "aram": True,
    },
    "Archangel's Staff": {
        "sr": True,
        "aram": True,
        "mana_charge": True,
        "limit": [
            "mana_charge"
        ],
    },
    "Ardent Censer": {
        "sr": True,
        "aram": True,
    },
    "Axiom Arc": {
        "sr": True,
        "aram": True,
    },
    "Banshee's Veil": {
        "sr": True,
        "aram": True,
    },
    "Black Cleaver": {
        "sr": True,
        "aram": True,
    },
    "Black Mist Scythe": {
        "sr": True,
        "aram": False,
        "support": True,
        "limit": [
            "support",
        ],
    },
    "Blade of the Ruined King": {
        "sr": True,
        "aram": True,
    },
    "Bloodthirster": {
        "sr": True,
        "aram": True,
    },
    "Bulwark of the Mountain": {
        "sr": True,
        "aram": False,
        "support": True,
        "limit": [
            "support",
        ],
    },
    "Chempunk Chainsword": {
        "sr": True,
        "aram": True,
    },
    "Chemtech Putrifier": {
        "sr": True,
        "aram": True,
    },
    "Cosmic Drive": {
        "sr": True,
        "aram": True,
    },
    "Dead Man's Plate": {
        "sr": True,
        "aram": True,
    },
    "Death's Dance": {
        "sr": True,
        "aram": True,
    },
    "Demonic Embrace": {
        "sr": True,
        "aram": True,
    },
    "Edge of Night": {
        "sr": True,
        "aram": True,
    },
    "Essence Reaver": {
        "sr": True,
        "aram": True,
    },
    "Force of Nature": {
        "sr": True,
        "aram": True,
    },
    "Frozen Heart": {
        "sr": True,
        "aram": True,
    },
    "Gargoyle Stoneplate": {
        "sr": True,
        "aram": True,
    },
    "Guardian Angel": {
        "sr": True,
        "aram": False,
    },
    "Guardian's Blade": {
        "sr": False,
        "aram": True,
        "guardian": True,
        "limit": [
            "guardian",
        ],
    },
    "Guardian's Hammer": {
        "sr": False,
        "aram": True,
        "guardian": True,
        "limit": [
            "guardian",
        ],
    },
    "Guardian's Horn": {
        "sr": False,
        "aram": True,
        "guardian": True,
        "limit": [
            "guardian",
        ],
    },
    "Guardian's Orb": {
        "sr": False,
        "aram": True,
        "guardian": True,
        "limit": [
            "guardian",
        ],
    },
    "Guinsoo's Rageblade": {
        "sr": True,
        "aram": True,
        "crit_modifier": True,
        "limit": [
            "crit_modifier"
        ],
    },
    "Horizon Focus": {
        "sr": True,
        "aram": True,
    },
    "Hullbreaker": {
        "sr": True,
        "aram": True,
    },
    "Infinity Edge": {
        "sr": True,
        "aram": True,
        "crit_modifier": True,
        "limit": [
            "crit_modifier"
        ],
    },
    "Knight's Vow": {
        "sr": True,
        "aram": True,
    },
    "Lich Bane": {
        "sr": True,
        "aram": True,
    },
    "Lord Dominik's Regards": {
        "sr": True,
        "aram": True,
        "last_whisper": True,
        "limit": [
            "last_whisper",
        ],
    },
    "Manamune": {
        "sr": True,
        "aram": True,
        "mana_charge": True,
        "limit": [
            "mana_charge"
        ],
    },
    "Maw of Malmortius": {
        "sr": True,
        "aram": True,
        "lifeline": True,
        "limit": [
            "lifeline",
        ],
    },
    "Mejai's Soulstealer": {
        "sr": True,
        "aram": False,
    },
    "Mercurial Scimitar": {
        "sr": True,
        "aram": True,
        "quicksilver": True,
        "limit": [
            "quicksilver",
        ],
    },
    "Mikael's Blessing": {
        "sr": True,
        "aram": True,
    },
    "Morellonomicon": {
        "sr": True,
        "aram": True,
    },
    "Mortal Reminder": {
        "sr": True,
        "aram": True,
    },
    "Nashor's Tooth": {
        "sr": True,
        "aram": True,
    },
    "Navori Quickblades": {
        "sr": True,
        "aram": True,
    },
    "Pauldrons of Whiterock": {
        "sr": True,
        "aram": False,
        "support": True,
        "limit": [
            "support",
        ],
    },
    "Phantom Dancer": {
        "sr": True,
        "aram": True,
    },
    "Rabadon's Deathcap": {
        "sr": True,
        "aram": True,
    },
    "Randuin's Omen": {
        "sr": True,
        "aram": True,
    },
    "Rapid Firecannon": {
        "sr": True,
        "aram": True,
    },
    "Ravenous Hydra": {
        "sr": True,
        "aram": True,
        "hydra": True,
        "limit": [
            "hydra",
        ],
    },
    "Redemption": {
        "sr": True,
        "aram": True,
    },
    "Runaan's Hurricane": {
        "sr": True,
        "aram": True,
        "range_exclusive": True,
    },
    "Rylai's Crystal Scepter": {
        "sr": True,
        "aram": True,
    },
    "Serpent's Fang": {
        "sr": True,
        "aram": True,
    },
    "Serylda's Grudge": {
        "sr": True,
        "aram": True,
        "last_whisper": True,
        "limit": [
            "last_whisper",
        ],
    },
    "Shadowflame": {
        "sr": True,
        "aram": True,
    },
    "Shard of True Ice": {
        "sr": True,
        "aram": False,
        "support": True,
        "limit": [
            "support",
        ],
    },
    "Silvermere Dawn": {
        "sr": True,
        "aram": True,
        "quicksilver": True,
        "limit": [
            "quicksilver",
        ],
    },
    "Spirit Visage": {
        "sr": True,
        "aram": True,
    },
    "Staff of Flowing Water": {
        "sr": True,
        "aram": True,
    },
    "Sterak's Gage": {
        "sr": True,
        "aram": True,
        "lifeline": True,
        "limit": [
            "lifeline",
        ],
    },
    "Stormrazor": {
        "sr": True,
        "aram": True,
    },
    "The Collector": {
        "sr": True,
        "aram": True,
    },
    "Thornmail": {
        "sr": True,
        "aram": True,
    },
    "Titanic Hydra": {
        "sr": True,
        "aram": True,
        "hydra": True,
        "limit": [
            "hydra",
        ],
    },
    "Umbral Glaive": {
        "sr": True,
        "aram": True,
    },
    "Vigilant Wardstone": {
        "sr": True,
        "aram": True,
    },
    "Void Staff": {
        "sr": True,
        "aram": True,
    },
    "Warmog's Armor": {
        "sr": True,
        "aram": True,
    },
    "Winter's Approach": {
        "sr": True,
        "aram": True,
        "mana_charge": True,
        "limit": [
            "mana_charge"
        ],
    },
    "Wit's End": {
        "sr": True,
        "aram": True,
    },
    "Youmuu's Ghostblade": {
        "sr": True,
        "aram": True,
    },
    "Zeke's Convergence": {
        "sr": True,
        "aram": True,
    },
    "Zhonya's Hourglass": {
        "sr": True,
        "aram": True,
    },
}

_BOOTS = {
    "Berserker's Greaves": {
        "sr": True,
        "aram": True,
        "boots": True,
        "limit": [
            "boots",
        ]
    },
    "Boots of Swiftness": {
        "sr": True,
        "aram": True,
        "boots": True,
        "limit": [
            "boots",
        ]
    },
    "Ionian Boots of Lucidity": {
        "sr": True,
        "aram": True,
        "boots": True,
        "limit": [
            "boots",
        ]
    },
    "Mercury's Treads": {
        "sr": True,
        "aram": True,
        "boots": True,
        "limit": [
            "boots",
        ]
    },
    "Mobility Boots": {
        "sr": True,
        "aram": True,
        "boots": True,
        "limit": [
            "boots",
        ]
    },
    "Plated Steelcaps": {
        "sr": True,
        "aram": True,
        "boots": True,
        "limit": [
            "boots",
        ]
    },
    "Sorcerer's Shoes": {
        "sr": True,
        "aram": True,
        "boots": True,
        "limit": [
            "boots",
        ]
    },
}

_MYTHIC_ITEMS = {
    "Crown of the Shattered Queen": {
        "sr": True,
        "aram": True,
    },
    "Divine Sunderer": {
        "sr": True,
        "aram": True,
    },
    "Duskblade of Draktharr": {
        "sr": True,
        "aram": True,
    },
    "Eclipse": {
        "sr": True,
        "aram": True,
    },
    "Evenshroud": {
        "sr": True,
        "aram": True,
    },
    "Everfrost": {
        "sr": True,
        "aram": True,
    },
    "Frostfire Gauntlet": {
        "sr": True,
        "aram": True,
    },
    "Galeforce": {
        "sr": True,
        "aram": True,
    },
    "Goredrinker": {
        "sr": True,
        "aram": True,
    },
    "Hextech Rocketbelt": {
        "sr": True,
        "aram": True,
    },
    "Immortal Shieldbow": {
        "sr": True,
        "aram": True,
        "lifeline": True,
        "limit": [
            "lifeline",
        ]
    },
    "Imperial Mandate": {
        "sr": True,
        "aram": True,
    },
    "Kraken Slayer": {
        "sr": True,
        "aram": True,
    },
    "Liandry's Anguish": {
        "sr": True,
        "aram": True,
    },
    "Locket of the Iron Solari": {
        "sr": True,
        "aram": True,
    },
    "Luden's Tempest": {
        "sr": True,
        "aram": True,
    },
    "Moonstone Renewer": {
        "sr": True,
        "aram": True,
    },
    "Night Harvester": {
        "sr": True,
        "aram": True,
    },
    "Prowler's Claw": {
        "sr": True,
        "aram": True,
    },
    "Riftmaker": {
        "sr": True,
        "aram": True,
    },
    "Shurelya's Battlesong": {
        "sr": True,
        "aram": True,
    },
    "Stridebreaker": {
        "sr": True,
        "aram": True,
    },
    "Sunfire Aegis": {
        "sr": True,
        "aram": True,
    },
    "Trinity Force": {
        "sr": True,
        "aram": True,
    },
    "Turbo Chemtank": {
        "sr": True,
        "aram": True,
    },
}

_ROLES = [
    "Top",
    "Jungle",
    "Middle",
    "Bottom",
    "Support",
]


class LeagueChampionItemConfig(object):
    """Represents a League of Legends item build for a champion

    Attributes:
    ------
    mythic :class:`str`
        - The name of the mythic item in the build

    legendaries :class:`List[str]`
        - The names of the 4-5 legendary items in the build

    boots :class:`Optional[str]`
        - The name (if any) of the boots in the build

    mode :class:`str`
        - The gamemode the build is meant to be used in (SR / ARAM)

    limit :class:`List[str]`
        - The list of limitations on itemisation imposed by current items in the build
    """
    __slots__ = [
        "mythic",
        "legendaries",
        "boots",
        "limit",
    ]

    def __init__(self, mythic: str = "", legendaries: List[str] = [], boots: str = "") -> None:
        self.mythic = mythic
        self.legendaries = legendaries
        self.boots = boots
        self.limit = []

    @classmethod
    def from_random(self, champion: "LeagueChampion", mode: str = None, limit: List[str] = []) -> "LeagueChampionItemConfig":
        if mode is None or mode.lower() not in ["sr", "aram"]:
            raise ValueError("Mode should be either \"SR\" or \"ARAM\"")
        mythic, mythic_data = random.choice([
            (name, data) for name, data in _MYTHIC_ITEMS.items() if data[mode.lower()]
        ])
        _limit = mythic_data.get("limit", limit)
        if champion.is_melee:
            _limit.append("range_exclusive")
        _legendary_loops = 4 if champion.id != "Cassiopeia" else 5
        available_legendaries = [
            (name, data) for name, data in _LEGENDARY_ITEMS.items() if data[mode.lower()] and not any(key in _limit for key in list(data.keys()))
        ]
        legendaries = []
        for _ in range(_legendary_loops):
            if "force_support" in _limit and not "support" in _limit:
                sel = random.choice([
                    (name, data) for name, data in available_legendaries if data.get("support")
                ])
            else:
                sel = random.choice(available_legendaries)
            legendaries.append(sel[0])
            available_legendaries.remove(sel)
            for _lim in (sel[1]).get("limit", []):
                _limit.append(_lim)
            available_legendaries = [
                (name, data) for name, data in available_legendaries if data[mode.lower()] and not any(key in _limit for key in list(data.keys()))
            ]

        boots = random.choice([
            name for name in list(_BOOTS.keys())
        ]) if _legendary_loops == 4 else None
        return self(mythic=mythic, legendaries=legendaries, boots=boots)


class LeagueChampionRuneConfig(object):
    """Represents a League of Legends rune configuration for a champion

    Attributes:
    ------
    primary_tree :class:`str`
        - The name of the configuration's primary tree

    keystone :class:`str`
        - The name of the keystone rune

    p1 :class:`str`
        - The name of the slot 1 rune of the primary tree

    p2 :class:`str`
        - The name of the slot 2 rune of the primary tree

    p3 :class:`str`
        - The name of the slot3 rune of the primary tree

    secondary_tree :class:`str`
        - The name of the configuration's secondary tree

    s1 :class:`str`
        - The name of the first rune selected from the secondary tree

    s2 :class:`str`
        - The name of the second rune selected from the secondary tree

    minor_runes :class:`List[str]`
        - The list of minor runes selected
    """
    __slots__ = [
        "primary_tree",
        "keystone",
        "p1",
        "p2",
        "p3",
        "secondary_tree",
        "s1",
        "s2",
        "minor_runes",
    ]

    def __init__(self, data: dict) -> None:
        self.primary_tree = data.get("primary_tree")
        self.keystone = data.get("keystone")
        self.p1 = data.get("p1")
        self.p2 = data.get("p2")
        self.p3 = data.get("p3")
        self.secondary_tree = data.get("secondary_tree")
        self.s1 = data.get("s1")
        self.s2 = data.get("s2")
        self.minor_runes = data.get("minor_runes")

    @classmethod
    def from_random(self) -> "LeagueChampionRuneConfig":
        primary = random.choice(list(_MAJOR_RUNES.keys()))
        keystone = random.choice(_MAJOR_RUNES[primary]["Keystones"])
        p1 = random.choice(_MAJOR_RUNES[primary]["1"])
        p2 = random.choice(_MAJOR_RUNES[primary]["2"])
        p3 = random.choice(_MAJOR_RUNES[primary]["3"])
        secondary = random.choice([tree for tree in _MAJOR_RUNES.keys() if tree != primary])
        eligible_secondary_selections = [
            rune_options for category, rune_options in _MAJOR_RUNES[secondary].items() if category != "Keystones"
        ]
        samples = random.sample(eligible_secondary_selections, 2)
        s1 = random.choice(samples[0])
        s2 = random.choice(samples[1])
        minor_runes = [random.choice(runes) for runes in _MINOR_RUNES.values()]
        return self({
            "primary_tree": primary,
            "keystone": keystone,
            "p1": p1,
            "p2": p2,
            "p3": p3,
            "secondary_tree": secondary,
            "s1": s1,
            "s2": s2,
            "minor_runes": minor_runes,
        })


class LeagueChampionSkin(object):
    """Represents a League of Legends champion skin

    Attributes:
    ------
    name :class:`str`
        - The name of the skin

    number :class:`int`
        - The skin's number]
    """
    __slots__ = [
        "name",
        "number",
    ]

    def __init__(self, name: str, number: int) -> None:
        self.name = name
        self.number = number

    @classmethod
    def from_data(self, data: dict) -> "LeagueChampionSkin":
        return self(name=data.get("name"), number=int(data.get("num")))


class LeagueChampion(object):
    """Represents League of Legends champion data for a particular summoner

    Attributes:
    ------
    version :class:`str`
        - The League of Legends patch number

    name :class:`str`
        - The champion's name

    id :class:`str`
        - The champion's ID (used in json file lookups)

    title :class:`str`
        - The champion's title

    is_melee :class:`bool`
        - Boolean representing if a champion is melee

    is_ranged :class:`bool`
        - Boolean representing if a champion is ranged

    skins :class:`List[LeagueChampionSkin]`
        - The list of all of the champion's skins

    splash_url :class:`str`
        - The URL of the champion's splash art

    square_url :class:`str`
        - The URL of the champion's square asset

    mastery_points :class:`int`
        - The amount of mastery points the summoner has on this champion

    mastery_level :class:`int`
        - The mastery level this summoner has on this champion
    """
    __slots__ = [
        "version",
        "name",
        "id",
        "title",
        "is_melee",
        "is_ranged",
        "skins",
        "splash_url",
        "square_url",
        "mastery_points",
        "mastery_level",
        "_last_rolled_skin"
    ]

    def __init__(self, version: str, data: dict, points: int = None, level: int = None) -> None:
        self.version = version
        self.name = data.get("name")
        self.id = data.get("id")
        self.title = data.get("title")
        self.is_melee = data["stats"]["attackrange"] <= 250
        self.is_ranged = not self.is_melee
        self.skins = [LeagueChampionSkin.from_data(skin_data) for skin_data in data.get("skins")]
        self.splash_url = _BASE_SPLASH_URL.format(self.id, 0)
        self.square_url = _BASE_SQUARE_ART_URL.format(self.version, self.id)
        self.mastery_points = points
        self.mastery_level = level
        self._last_rolled_skin = f"Classic {self.name}"

    def get_skin_splash(self, number: int = 0, randomise: bool = False) -> str:
        if randomise:
            skin = random.choice(self.skins)
        else:
            skin = [_skin for _skin in self.skins if _skin.number == number][0]
        self._last_rolled_skin = skin.name if skin.name != "default" else f"Classic {self.name}"
        return _BASE_SPLASH_URL.format(self.id, skin.number)
