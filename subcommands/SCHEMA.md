# Subcommand Help Schema
MarwynnBot's subcommand help data shall be stored in the file 
[`subcommandhelp.json`](./subcommandhelp.json).
This document outlines the JSON schema that shall be used in order to ensure
consistency and make writing and reading data predictable.

# Schema
```json
{
    "base_command_name": {
        "subcommand_name": {
            "desc": "description",
            "usage": "name",
            "returns": "result of the subcommand invocation",
            "aliases": [
                "list",
                "of",
                "aliases",
                "for",
                "the",
                "subcommand",
                "name",
                "only"
            ] | null,
            "uperms": [
                "permission",
                ...
            ],
            "bperms": [
                "permission",
                ...
            ],
            "note": "additional information" | null
        },
        ...
    },
    ...
}
```
*... represents additional elements of the same schema as its parent*

## Required Elements
These elements must exist in every subcommand help object.

> - `base_command_name` must be lowercase
> - `subcommand_name` must be title cased, and should never be more than one word.
> If it is more than one word, ask Marwynn
> - `desc` describes what the subcommand does
> - `usage` must be lowercase. Notice how usage in current code is never the
> full command invocation, rather the subcommand fragment of the full command
> invocation
> - `returns` should mention the result of the subcommand if it executes successfully

## Optional Elements
These elements may not be required, and should not be specified if they aren't.
The null in the schema specification is there just to show that it is possible
to not include it.

> - `aliases` must be a list of lowercase aliases. Separate each element in the
> list with a newline and do trailing commas for all elements except for the last
> element
> - `uperms` and `bperms` are a list of discord permissions, where the `u` stands for
> "user" and `b` stands for "bot". Trailing comma rule is the same as for `aliases`
> - `note` should be additional information relevant to the command, such as
> more information regarding a parameter the subcommand accepts or required
> permissions to use the subcommand

# Applications
This file will be used as the data for all subcommands MarwynnBot features.
Subcomnmand help and command info should pull data from this file and this
file only. Once data has been migrated to this file, a patch will be released
in the bot's code that ensures consistency and complete documentation for all
subcommands.

Since this schema defines a JSON file, this information may be retrieved through
a REST API, and hopefully an GraphQL version in the future, as that allows for
the user to define their own return schema instead of being forced to use a
fixed schema defined by the server.