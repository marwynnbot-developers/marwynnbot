# MarwynnBot <img src="https://i.kym-cdn.com/photos/images/original/000/762/243/edd.jpg" height="100" width="100" align="right">
A feature rich Discord bot that can do almost anything. MarwynnBot is the perfect solution to all your Discord needs.
I am currently under development, so please expect new features to come and any issues
to be addressed as soon as I can.

## Add Me To Your Server
You can add the public MarwynnBot to your own Discord server by clicking
[here](https://discord.com/oauth2/authorize?client_id=623317451811061763&scope=bot&permissions=2146958583)

## Prefix
- The default prefix is `m!`
- Global prefixes are mentioning the bot or `mb` *(case insensitive)*

## Commands and Info
All of MarwynnBot's commands are listed on its website, https://bot.marwynn.me.
Invite the bot and do `m!help` to see all commands and their usages inside Discord.

## Self Hosting
I would prefer that you not create an instance of this bot/repo for self hosting
purposes. This repository is public only for educational purposes. If you choose
to self host, you are on your own. I will not be providing any help or support
for those who choose to self host. If you would like to use MarwynnBot, please
use the [invite link](https://discord.com/oauth2/authorize?client_id=623317451811061763&scope=bot&permissions=2146958583)
).

## Contact Info ![Discord](https://img.shields.io/discord/707981159748993084?color=blue&label=Discord)
**Discord:** MS Arranges#3060

**Discord Server:** https://discord.gg/78XXt3Q